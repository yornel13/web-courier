import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import {Admin} from '../../model/admin/admin';
import {ApiResponse} from '../../model/app.response';
import {Project} from '../../model/proyect/project';
import { UserService } from 'src/app/_services/firebaseRoleServices/connectionServices/user.service';
import { User } from 'src/app/_services/firebaseRoleServices/fireModels/user';

const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private user: Admin;
    private listUser = [];
    private readonly TOKEN_SESSION = 'TOKEN-SESSION';
    private readonly TOKEN_FIRE = 'TOKEN-FIRE';
    private readonly USER = 'USER-ADMIN';
    private readonly SELECTED_COMPANY_ID = 'SELECTED-COMPANY-ID';
    private readonly PROJECT = 'SELECTED-PROJECT';

    constructor(
          private http: HttpClient,
          private userService: UserService
          ) { }

    public getTokenSession(): string {
        return localStorage.getItem(this.TOKEN_SESSION);
    }

    public getTokenFire(): string {
        return localStorage.getItem(this.TOKEN_FIRE);
    }

    public getUser(): Admin {
        return JSON.parse(localStorage.getItem(this.USER));
    }

    public getProject(): Project {
        return JSON.parse(localStorage.getItem(this.PROJECT));
    }

    public setTokenSession(tokenSession: string): boolean {
        localStorage.setItem(this.TOKEN_SESSION, tokenSession);
        return true;
    }

    public setTokenFire(tokenFire: string): boolean {
        localStorage.setItem(this.TOKEN_FIRE, tokenFire);
        return true;
    }

    public setUser(user: string): boolean {
        //console.log(user);
        localStorage.setItem(this.USER, user);
        // this.userService.getUsers().subscribe(users => {
        //     this.listUser = users;
        //     for (let i = 0; i < this.listUser.length; i ++) {
        //         if (this.listUser[i].dni === this.user.dni && this.listUser[i].name === this.user.name) {
        //             localStorage.setItem('ROLE',  this.listUser[i].rol );
        //             break;
        //         }
        //     }
        // });
        return true;
    }

    public setSelectedCompany(id: number): boolean {
        localStorage.setItem(this.SELECTED_COMPANY_ID, id + '');
        return true;
    }

    public setProject(object: Project): boolean {
        localStorage.setItem(this.PROJECT, JSON.stringify(object));
        return true;
    }

    public getSelectedCompany(): number {
        const id = +localStorage.getItem(this.SELECTED_COMPANY_ID);
        return id === null ? 0 : +id;
    }

    public cleanStore(): boolean {
        localStorage.removeItem(this.TOKEN_SESSION);
        localStorage.removeItem(this.USER);
        return true;
    }

    login(dni: string, password: string) {
        return this.http.post(`${environment.BASIC_URL}/auth/admin`,
            {
                dni: dni,
                password: password
            })
            .toPromise().then((response) => response);
    }

    verify(tokenSession: string) {
        this.setTokenSession(tokenSession);
        return this.http.get(`${environment.BASIC_URL}/auth/verify`)
            .pipe(map((data: Admin) => {
                if (data !== null && data !== undefined) {
                    this.user = data;
                    this.setUser(JSON.stringify(data));
                }
            })).toPromise().then((response) => response);
    }

    logout() {
        return this.http.post(`${environment.BASIC_URL}/auth/logout`,
            null)
            .pipe(map((data: ApiResponse) => {
                console.log(data);
            })).toPromise().then((response) => response);
    }

    webRegister(tokenFire: String, tokenSession: String, adminId: number) {
        return this.http.post(`${environment.BASIC_URL}/messenger/register/web`,
            {
                registration_id: tokenFire,
                session: tokenSession,
                admin_id: adminId
            })
        .toPromise().then((response) => response);
    }

    getHeader() {
        return new HttpHeaders();
    }
}
