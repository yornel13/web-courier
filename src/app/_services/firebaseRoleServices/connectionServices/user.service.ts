import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {User, Permission} from '../fireModels/user';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    usersCollection: AngularFirestoreCollection<User>;
    users: Observable<User[]>;
    userDoc: AngularFirestoreDocument<User>;
    constructor(public db: AngularFirestore) {
        this.usersCollection = this.db.collection('user');
        this.users = this.usersCollection.snapshotChanges().pipe(map(actions => {
            return actions.map(a => {
                const data = a.payload.doc.data() as User;
                data.id = a.payload.doc.id;
                return data;
            });
        }));
    }
    getUsers() {
        return this.users;
    }

    addUser(user: User) {
        this.usersCollection.add(user);
    }

    deleteUser(user: User) {
        this.userDoc = this.db.doc(`users/${user.id}`);
        this.userDoc.delete();
    }

    updateUser(user: User) {
        this.userDoc = this.db.doc(`users/${user.id}`);
        this.userDoc.update(user);
    }
}

