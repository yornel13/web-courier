export interface User{
    id?: string;
    dni?: string;
    name?: string;
    lastname?: string;
    rol?: string;
    registered_by?: number;
    company_id?: number;
    permissions: Permission[]; 
}

export interface Permission {
    enabled: boolean;
    see: boolean;
    edit: boolean;
    name: string;
}