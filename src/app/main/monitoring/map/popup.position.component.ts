import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-position-alert',
    templateUrl: './popup.position.component.html',
    styleUrls: ['./popup.position.css']
})
export class PopupPositionComponent implements OnInit {

    position: any;
    reportIcon = './assets/alerts/report.png';

    constructor() {}

    ngOnInit() {
        if (this.position.message === 'INCIDENCE_LEVEL_1'
            || this.position.message === 'INCIDENCE_LEVEL_2') {
            this.position.hasReport = true;
        }
    }
}
