import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../../environments/environment';
import { ProjectService } from '../../../../model/proyect/project.service';
import { Project } from '../../../../model/proyect/project';
import { Point } from '../../../../model/point/point';
import { Contact } from '../../../../model/contact/contact';
import { Order } from '../../../../model/order/order';
import { Route } from '../../../../model/route/route';
import { OrderContact } from '../../../../model/contact/order.contact';
import { OrderService } from '../../../../model/order/order.service';
import { RouteService } from '../../../../model/route/route.service';
import { OrderContactService } from '../../../../model/contact/order.contact.service';
import { PositionService } from '../../../../model/position/position.service';
import { PositionsUtils } from '../../../../model/position/positions.utils';
import { MainService } from '../../main.service';
import { Vehicle } from '../../../../model/vehicle/vehicle';
import { AuthenticationService } from '../../../_services';
import * as L from 'leaflet';
import { GlobalOsm } from '../../../global.osm';
import { LatLng } from 'leaflet';
import { AssignedDriverService } from '../../../../model/guard/assigned.driver.service';
import { AssignedDriver } from '../../../../model/guard/assigned.driver';
import { Position } from '../../../../model/position/position';
import { BinnacleService } from '../../../../model/binnacle/binnacle.service';
import { Binnacle } from '../../../../model/binnacle/binnacle';
import * as uuid from 'uuid';
import { IncidenciasService } from '../../../../model/incidence/incidence.service';
import { AssignedLoaderService } from '../../../../model/loader/assigned.loader.service';
import * as jsPDF from 'jspdf';
import { ExcelService } from '../../../../model/excel/excel.services';
import { AssignedLoader } from '../../../../model/loader/assigned.loader';
import { VehistorialService } from '../../../../model/historial/vehistorial.service';

@Component({
    selector: 'app-tracing',
    templateUrl: './tracing.component.html',
    styleUrls: ['./tracing.component.css']
})

export class TracingComponent implements OnInit {

    @ViewChild('buttonOpenModal') private buttonOpenModal: ElementRef;
    @ViewChild('buttonCloseModal') private buttonCloseModal: ElementRef;

    @Input()
    showMarker;

    @Input()
    vehicles: Vehicle[] = [];

    dataProjects: Project[] = [];
    keyProjects = 'id'; // set default
    filterProjects: any;
    reverseProjects = true;
    searchingProjects = false;

    keyVehicles = 'alias'; // set default
    filterVehicles: string;
    reverseVehicles = true;

    dataOrders: any[] = [];
    keyOrders = 'id'; // set default
    filterOrders: string;
    reverseOrders = true;

    dataRoutes: Point[] = [];
    keyRoutes = 'id'; // set default
    filterRoutes: string;
    reverseRoutes = true;
    searchingRoutes = false;

    dataContacts: Contact[] = [];
    keyContact = 'id'; // set default
    filterContacts: string;
    reverseContacts = true;

    dataSites: any[] = [];
    keySites = 'no'; // set default
    reverseSites = true;

    dataDeliveries: any[] = [];
    keyDeliveries = 'type'; // set default
    reverseDeliveries = true;

    dataPositions: any[] = [];
    dataPositionsToEdit: any[] = [];
    keyPositions = 'index'; // set default
    reversePositions = true;
    searchingPositions = false;

    kilometers = '0';

    assignedDrivers: AssignedDriver[] = [];
    date;
    point: {
        latitude?: string,
        longitude?: string,
        driverId?: number,
        routeId?: number,
        incidence?: any,
        message?: string,
        alertMessage?: string,
        date?: string,
        time?: string,
        custom?: boolean
    } = {};

    selectInput = [
        {value: 'UPDATE', txt: 'Actualización  de posición'},
        {value: 'STARTED_TRACKING', txt: 'Comienzo de tracking'},
        {value: 'PAUSE_TRACKING', txt: 'Tracking pausado'},
        {value: 'RESUMED_TRACKING', txt: 'Tracking retomado'},
        {value: 'FINISHED_TRACKING', txt: 'Tracking finalizado'},
        {value: 'PICKUP', txt: 'Carga PICKUP'},
        {value: 'DELIVERY', txt: 'Carga Entregada'},
        {value: 'INCIDENCE_LEVEL_1', txt: 'Incidencia'},
        {value: 'SOS', txt: 'SOS'}
    ];

    timePoint = {hour: 13, minute: 30};
    meridianPoint = true;


    hide = false;

    incidences: any[] = [];
    project: Project = {};
    order: any = {};
    routes: Route[] = [];
    contacts: OrderContact[] = [];
    containsOrigin = true;
    containsOrder: boolean;

    assignedLoaders = [];

    // new 8a
    latitude: '-2.071522';
    longitude: '-79.607105';
    screen = 1;
    map: L.Map;
    zoom: number;
    center = L.latLng(([-2.071522, -79.607105]));
    layersControlOptions;
    baseLayers;
    options;

    constructor(
        private authService: AuthenticationService,
        private projectService: ProjectService,
        private orderService: OrderService,
        private routeService: RouteService,
        private contactService: OrderContactService,
        private assignedLoaderService: AssignedLoaderService,
        private positionService: PositionService,
        private binnacleService: BinnacleService,
        private mainService: MainService,
        private toastr: ToastrService,
        private router: Router,
        private assignDriverService: AssignedDriverService,
        private incidencesService: IncidenciasService,
        private globalOSM: GlobalOsm,
        private excelService: ExcelService,
        private veHistoryService: VehistorialService) {
        this.layersControlOptions = this.globalOSM.layersOptions;
        this.baseLayers = this.globalOSM.baseLayers;
        this.options = this.globalOSM.defaultOptions;
        this.zoom = this.globalOSM.zoom;
    }

    ngOnInit() {
        this.date = new Date().toISOString().split('T')[0];
        this.getAllProjects();
        this.mainService.pointerSelectedEmitter.subscribe((coors: LatLng) => {
            this.onPointMarkerSelected(coors);
        });
    }

    // TODO, permission static
    hasFullPermission() {
        const user = this.authService.getUser();
        return (user && user.rol && (user.rol === 'MASTER' || user.rol === 'ADMIN'));
    }

    sortProjects(key) {
        this.keyProjects = key;
        this.reverseProjects = !this.reverseProjects;
        if (key == 'status') {
            this.filterProjectByStatus();
        } else if (key == 'name') {
            this.filterProjectByName();
        } else if (key == 'create_at') {
            this.filterProjectByCreateAt();
        } else if (key == 'shipment_date') {
            this.filterProjectByShipmentDate();
        }
    }

    sortOrders(key) {
        this.keyOrders = key;
        this.reverseOrders = !this.reverseOrders;
    }

    sortRoutes(key) {
        this.keyRoutes = key;
        this.reverseRoutes = !this.reverseRoutes;
    }

    sortSites(key) {
        this.keySites = key;
        this.reverseSites = !this.reverseSites;
    }

    sortDeliveries(key) {
        this.keyDeliveries = key;
        this.reverseDeliveries = !this.reverseDeliveries;
    }

    getAllProjects() {
        this.searchingProjects = true;
        this.dataProjects = [];
        this.projectService.getAll().then(
            (success: any) => {
                this.searchingProjects = false;
                this.dataProjects = success.data;
            }, error => {
                this.searchingProjects = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    openProjects() {
        this.router.navigate(['u/control/proyectos']).then();
    }

    openRoutes() {
        this.router.navigate(['u/control/puntos']).then();
    }

    openContacts() {
        this.router.navigate(['u/control/contactos']).then();
    }

    showDetails(d) {
        this.kilometers = '0';
        this.hide = true;
        this.screen = 2;
        this.project = d;
        this.order = {};
        this.routes = [];
        this.contacts = [];
        this.getOrder();
        this.getIncidences();
        this.getOrderRoutes();
        this.getOrderContacts();
        this.getAssignedLoaders();
        this.getAssignedDrivers();
    }

    getOrder() {
        this.order = {};
        this.containsOrder = undefined;
        this.orderService.getByProject(this.project.id)
            .then(
                (success: Order) => {
                    this.containsOrder = true;
                    this.order = success;
                }, error => {
                    if (error.status !== 404) {
                        this.toastr.info(environment.ERROR_GENERAL, 'Error',
                            {positionClass: 'toast-bottom-center'});
                    } else {
                        this.containsOrder = false;
                    }
                }
            );
    }

    getOrderRoutes() {
        this.routes = [];
        this.containsOrigin = true;
        this.searchingRoutes = true;
        this.routeService.getByProject(this.project.id).then(
            (success: any) => {
                this.searchingRoutes = false;
                this.routes = success.data;
                this.containsOrigin = false;
                this.routes.forEach(route => {
                    if (route.type === 'ORIGIN') {
                        this.containsOrigin = true;
                    }
                });
                this.mainService.routesOrderEmitter.emit(this.routes);
            }, error => {
                this.searchingRoutes = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    getOrderContacts() {
        this.containsOrigin = true;
        this.contactService.getByProject(this.project.id).then(
            (success: any) => {
                this.contacts = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    getAssignedLoaders() {
        this.assignedLoaders = [];
        this.assignedLoaderService.getByProjectId(this.project.id).then(
            (success: any) => {
                this.assignedLoaders = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    calculateKilometers() {
        let sumKilometers = 0;
        let lastPosition = null;
        this.dataPositions.forEach(position => {
            if (this.dataPositions.indexOf(position) !== 0) {
                sumKilometers = sumKilometers + this.calculateDistance(
                    position.latitude, position.longitude,
                    lastPosition.latitude, lastPosition.longitude);
            }
            lastPosition = position;
        });
        this.kilometers = sumKilometers.toFixed(1);
    }

    calculateDistance(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    deg2rad(deg) {
        return deg * (Math.PI / 180);
    }

    getAssignedDrivers() {
        this.assignDriverService.getByProjectId(this.project.id).then(
            (success: any) => {
                this.assignedDrivers = success.data;
                this.getPositions();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    getPositions() {
        this.dataPositions = [];
        this.searchingPositions = true;
        this.positionService.getByProject(this.project.id).then(
            (success: any) => {
                this.getClaroHistory(success.data);
            }, error => {
                this.searchingPositions = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    getClaroHistory(phonePositions: Position[]) {
        if (this.assignedDrivers && this.assignedDrivers.length === 1 && this.assignedDrivers[0].claro_vehicle && phonePositions && phonePositions.length > 1) {
            const firstDate = new Date(phonePositions[0].generated_time);
            const lastDate = new Date(phonePositions[phonePositions.length - 1].generated_time/*.replace('10', '13')*/);
            const claroVehicle = this.assignedDrivers[0].claro_vehicle;
            this.veHistoryService.superQuery(firstDate, lastDate, claroVehicle.imei).then(success => {
                this.dataPositions = new PositionsUtils().processPositionsAndHistory(phonePositions, success);
                this.mainService.recordsOrderEmitter.emit(this.dataPositions);
                this.calculateKilometers();
                this.searchingPositions = false;
            });
        } else {
            this.dataPositions = new PositionsUtils().processPositionsFake(phonePositions);
            this.mainService.recordsOrderEmitter.emit(this.dataPositions);
            this.calculateKilometers();
            this.searchingPositions = false;
        }
    }

    getIncidences() {
        this.incidencesService.getAll().then(
            (success: any) => {
                this.incidences = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    showDetailsView(project) {
        this.router.navigate(['/u/control/proyectos/' + project.id + '/logistica']).then();
    }

    focusVehicle(vehicle: Vehicle) {
        const latlng = {lat: vehicle.latitude, lng: vehicle.longitude};
        this.mainService.marker.emit(latlng);
        this.mainService.vehicle.emit(vehicle);
    }

    focusPosition(position: any) {
        const latlng = {lat: position.latitude, lng: position.longitude};
        this.mainService.marker.emit(latlng);
    }

    showMain() {
        this.hide = !this.hide;
        this.screen = 1;
        this.mainService.showVehicles.emit(true);
    }

    showBinnacle(project) {
        this.authService.setProject(project);
        this.router.navigate(['/u/control/proyectos/' + project.id + '/bitacora']).then();
    }

    editHistory() {
        this.screen++;
        this.getPositionsToEdit();
    }

    getPositionsToEdit() {
        this.dataPositionsToEdit = [];
        this.searchingPositions = true;
        this.positionService.getByProjectAll(this.project.id).then(
            (success: any) => {
                this.searchingPositions = false;
                this.dataPositionsToEdit = new PositionsUtils().processPositionsFake(success.data);
                this.mainService.recordsOrderEmitter.emit(this.dataPositionsToEdit);
            }, error => {
                this.searchingPositions = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    backToDetails() {
        this.hide = !this.hide;
        this.screen = 2;
        this.getPositions();
        this.mainService.recordsOrderEmitter.emit(this.dataPositions);
    }

    createPoint() {
        this.mainService.createPointEmitter.emit(true);
    }

    onPointMarkerSelected(latLng: LatLng) {
        this.buttonOpenModal.nativeElement.click();
        this.point = {
            latitude: String(latLng.lat),
            longitude: String(latLng.lng),
            driverId: undefined,
        };
    }

    saveCustomPosition() {
        if (this.point.message == undefined) {
            this.toastr.warning('Debe seleccionar el tipo', 'Aviso',
                {positionClass: 'toast-bottom-center'});
            return;
        }
        if (this.point.date == undefined) {
            this.toastr.warning('Debe seleccionar la fecha', 'Aviso',
                {positionClass: 'toast-bottom-center'});
            return;
        }
        if (this.point.time == undefined) {
            this.toastr.warning('Debe seleccionar la hora', 'Aviso',
                {positionClass: 'toast-bottom-center'});
            return;
        }
        if (this.point.driverId == undefined) {
            this.toastr.warning('Debe seleccionar un conductor', 'Aviso',
                {positionClass: 'toast-bottom-center'});
            return;
        }
        const positionTime = this.point.date + ' ' + this.point.time + ':00';
        const isException = this.point.message !== PositionsUtils.UPDATE;
        const randomId: string = uuid.v4();
        const position: Position = {
            driver_id: this.point.driverId,
            project_id: this.project.id,
            route_id: this.point.routeId,
            latitude: this.point.latitude,
            longitude: this.point.longitude,
            generated_time: positionTime,
            message_time: positionTime,
            is_exception: isException,
            imei: '',
            message: this.point.message,
            alert_message: this.point.alertMessage,
            custom: true
        };
        if (position.is_exception) {
            position.binnacle_sync_id = randomId;
        }
        this.positionService.sync(position).then(
            (success: any) => {
                if (position.is_exception) {
                    this.saveReport(success.result);
                } else {
                    this.buttonCloseModal.nativeElement.click();
                    this.toastr.success('La ubicacion ha sido agregada con exito!.', '',
                        {positionClass: 'toast-bottom-center'});
                    this.getPositions();
                }
            }, error => {
                this.toastr.warning(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    saveReport(position: Position) {
        const binnacle: Binnacle = {
            title: position.message === PositionsUtils.INCIDENCE_LEVEL_1 ? this.point.incidence.name : position.message,
            observation: position.alert_message ? position.alert_message : position.message,
            create_at: position.generated_time,
            update_at: position.generated_time,
            latitude: position.latitude,
            longitude: position.longitude,
            level: position.message === PositionsUtils.INCIDENCE_LEVEL_1 ? this.point.incidence.level : 1,
            guard_id: position.driver_id,
            project_id: position.project_id,
            incidence_id: position.message === PositionsUtils.INCIDENCE_LEVEL_1 ? this.point.incidence.id : undefined,
            status: 1,
            resolved: 0,
            sync_id: position.binnacle_sync_id,
        };

        this.binnacleService.sync(binnacle).then(
            (success: any) => {
                this.buttonCloseModal.nativeElement.click();
                this.toastr.success('La ubicacion ha sido agregada con exito!.', '',
                    {positionClass: 'toast-bottom-center'});
                this.getPositionsToEdit();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    deletePointMarker(position: Position) {
        this.positionService.delete(position.id).then(
            (success: any) => {
                this.toastr.success('La ubicacion ha sido eliminada con exito!.', '',
                    {positionClass: 'toast-bottom-center'});
                this.getPositionsToEdit();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    {positionClass: 'toast-bottom-center'});
            }
        );
    }

    onChangeDriver(value: any) {
        console.log('driver id changed', value);
        this.point.driverId = value;
    }

    generatePDF() {
        var doc = new jsPDF();
        doc.setFontSize(20);
        doc.text('Log Courier', 15, 20);
        doc.setFontSize(12);
        doc.setTextColor(100);
        var d = new Date();
        var fecha = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        doc.text('Proyecto: ' + this.project.name, 15, 30);
        doc.text('Hora de impresión: ' + fecha, 15, 37);
        //inserting data

        let align = 37;
        doc.setTextColor(0);
        doc.setFontType('bold');
        if (this.assignedLoaders.length > 0) {
            align += 7;
            doc.text('Estibas: ', 15, align += 7);
            doc.setFontType('normal');
            this.assignedLoaders.forEach((assigned: AssignedLoader) => {
                doc.text(assigned.loader.dni + ', ' + assigned.loader.name + ' ' + assigned.loader.name, 15, align += 7);
            });
        }
        align += 7;
        doc.setFontType('bold');
        doc.text('Kilometros Recorridos: ', 15, align += 7);
        doc.setFontType('normal');
        doc.text(this.kilometers + 'km', 70, align);
        // //
        doc.setFontType('bold');
        doc.text('Provincia: ', 15, align += 7);
        doc.setFontType('normal');
        doc.text(this.order.region, 40, align);
        // //
        doc.setFontType('bold');
        doc.text('Ciudad: ', 15, align += 7);
        doc.setFontType('normal');
        doc.text(this.order.city, 38, align);
        // //
        doc.setFontType('bold');
        doc.text('Centro de costo: ', 15, align += 7);
        doc.setFontType('normal');
        doc.text(this.order.cost_center, 50, align);
        // //
        doc.setFontType('bold');
        doc.text('Creado: ', 15, align += 7);
        doc.setFontType('normal');
        doc.text(this.order.create_at, 40, align);
        // //
        let state = 'En espera';
        if (this.project.status == 'A' && this.project.percentage == 0) {
            state = 'En espera;';
        } else if (this.project.status == 'A' && this.project.percentage > 0 && this.project.percentage < 100) {
            state = String(this.project.percentage) + '%';
        } else if (this.project.status == 'A' && this.project.percentage == 100) {
            state = 'Completado';
        } else if (this.project.status != 'A') {
            state = 'Inactivo';
        }
        doc.setFontType('bold');
        doc.text('Estado: ', 15, align += 7);
        doc.setFontType('normal');
        doc.text(state, 35, align);
        return doc;
    }

    downloadPDF() {
        this.generatePDF().save('proyecto_' + this.project.id + '.pdf');
    }

    printPDF() {
        const doc = this.generatePDF();
        doc.autoPrint();
        window.open(doc.output('bloburl'), '_blank');
    }

    convertTime(time: string) {
        const res = time.substring(0, 2);
        let resNumber = Number(res);
        if (resNumber > 12) {
            resNumber = resNumber - 12;
            time = String(resNumber) + time.substring(2, time.length) + ' PM';
        } else {
            time = time + ' AM';
        }
        return time;
    }

    filterProjectByStatus() {
        const sortedArray: Project[] = this.dataProjects.sort((obj1, obj2) => {
            if (obj1.percentage > obj2.percentage) {
                return 1;
            }
            if (obj1.percentage < obj2.percentage) {
                return -1;
            }
            return 0;
        });
        this.dataProjects = this.reverseProjects ? sortedArray.reverse() : sortedArray;
    }

    filterProjectByName() {
        const sortedArray: Project[] = this.dataProjects.sort((obj1, obj2) => (obj1.name > obj2.name ? -1 : 1));
        this.dataProjects = this.reverseProjects ? sortedArray : sortedArray.reverse();
    }

    filterProjectByCreateAt() {
        const sortedArray: Project[] = this.dataProjects.sort((obj1, obj2) =>
            (new Date(obj1.create_at).getTime() > new Date(obj2.create_at).getTime() ? -1 : 1));
        this.dataProjects = this.reverseProjects ? sortedArray : sortedArray.reverse();
    }

    filterProjectByShipmentDate() {
        const sortedArray: Project[] = this.dataProjects.sort((obj1, obj2) =>
            (new Date(obj1.shipment_date).getTime() > new Date(obj2.shipment_date).getTime() ? -1 : 1));
        this.dataProjects = this.reverseProjects ? sortedArray : sortedArray.reverse();
    }
}
