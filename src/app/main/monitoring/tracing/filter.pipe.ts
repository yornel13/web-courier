import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})

export class FilterPipe implements PipeTransform {
    transform(items: any[], search: any): any {
        if (search === undefined) {
            return items;
        } else {
            return items.filter(item => {
                return item.name.toLowerCase().includes(search.toLowerCase());
            });
        }
    }
}
