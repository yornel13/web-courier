import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import 'leaflet.markercluster';
import 'leaflet-draw';
import {PointService} from '../../../../model/point/point.service';
import {ToastrService} from 'ngx-toastr';
import {RegionsService} from '../../../../model/regions/regions.service';
import {City} from '../../../../model/regions/city';
import {Region} from '../../../../model/regions/region';
import {environment} from '../../../../environments/environment';
import {ProjectService} from '../../../../model/proyect/project.service';
import {Project} from '../../../../model/proyect/project';
import {Order} from '../../../../model/order/order';
import {OrderService} from '../../../../model/order/order.service';
import {Contact} from '../../../../model/contact/contact';
import {ContactService} from '../../../../model/contact/contact.service';
import {Point} from '../../../../model/point/point';
import {Route} from '../../../../model/route/route';
import {RouteService} from '../../../../model/route/route.service';
import {OrderContact} from '../../../../model/contact/order.contact';
import {OrderContactService} from '../../../../model/contact/order.contact.service';
import {AuthenticationService} from '../../../_services';
import {AssignedDriver} from '../../../../model/guard/assigned.driver';
import {GuardService} from '../../../../model/guard/guard.service';
import {Guard} from '../../../../model/guard/guard';
import {AssignedDriverService} from '../../../../model/guard/assigned.driver.service';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {
    // General
    projectId;
    project: Project = {};
    routes: Route[] = [];
    data: Order[] = [];
    isLoading = false;

    key: string = 'id'; //set default
    reverse: boolean = false;
    filter: string;

    constructor (
        private orderContactService: OrderContactService,
        private pointService: PointService,
        private contactService: ContactService,
        private orderService: OrderService,
        private projectService: ProjectService,
        private route: ActivatedRoute,
        private routeService: RouteService,
        private regionsService: RegionsService,
        private authService: AuthenticationService,
        private driverService: GuardService,
        private assignDriverService: AssignedDriverService,
        private toastr: ToastrService) {
    }

    ngOnInit() {
        this.project = this.authService.getProject();
        this.route.url.subscribe(value => {
            const projectId = value[value.length - 2].path;
            if (Number(projectId)) {
                this.projectId = projectId;
                this.getProject();
            }
        });
    }

    getProject() {
        this.isLoading = true;
        this.projectService.getId(this.projectId).then(
            (success: Project) => {
                this.project = success;
                this.getOrders();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getOrders() {
        this.isLoading = true;
        this.data = [];
        this.orderService.getByProject(this.projectId)
            .then(
            (success: any) => {
                this.data = success.data;
                this.isLoading = false;
            }, error => {
                this.isLoading = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

}
