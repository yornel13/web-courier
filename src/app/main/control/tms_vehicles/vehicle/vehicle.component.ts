import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ExcelService } from '../../../../../model/excel/excel.services';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../../environments/environment';
import {finalize} from 'rxjs/operators';
import {AngularFireStorage} from 'angularfire2/storage';
import {Observable} from 'rxjs';
import {CourierVehicleService} from '../../../../../model/courier-vehicle/courier-vehicle.service';
import {CourierVehicle} from '../../../../../model/courier-vehicle/courier-vehicle';

@Component({
    selector: 'app-vehicle',
    templateUrl: './vehicle.component.html',
    styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

    data: any[] = [];
    isLoading = false;

    // image upload
    photo: string;
    uploadPercent: Observable<number>;
    downloadURL: Observable<string>;

    // Vehicles view
    listView: boolean;
    showView: boolean;
    createView: boolean;
    editView: boolean;

    // Create View
    idEdit: number;
    alias: string;
    type: string;
    brand: string;
    color: string;
    plate: string;

    // General;
    vehicle: any = [];
    //edit
    placa:string;
    vehiculo:string;
    modelo:string;
    tipo:string;
    errorEdit:boolean = false;
    errorEditData:boolean = false;
    errorEditMsg:string;
    //eliminar
    errorDelete:boolean = false;
    errorDeleteData:boolean = false;
    filter:string;
    numElement:number = 10;
    //exportaciones
    contpdf:any = [];
    info: any = [];

    key: string = 'id'; // set default
    reverse: boolean = true;

    constructor(public router: Router,
                private vehicleService: CourierVehicleService,
                private excelService: ExcelService,
                private storage: AngularFireStorage,
                private toastr: ToastrService) {
        this.returnList();
    }

    returnList() {
        this.listView = true;
        this.showView = false;
        this.createView = false;
        this.editView = false;
        this.errorEditData = false;
    }

    ngOnInit() {
        this.getAll();
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    getAll() {
        this.isLoading = true;
        this.data = [];
        this.vehicleService.getAll().then(
            (success: any) => {
                this.data = success.data;
                const body = [];
                const excel = [];
                for (let i = 0; i < this.data.length; i++) {
                    this.data[i].id = Number(this.data[i].id);
                    excel.push({'#' : this.data[i].id, 'Placa': this.data[i].plate,
                        'Vehiculo': this.data[i].vehicle, 'Marca': this.data[i].brand, 'Color': this.data[i].type});
                    body.push([this.data[i].id, this.data[i].plate, this.data[i].vehicle, this.data[i].brand, this.data[i].type]);
                }
                this.contpdf = body;
                this.info = excel;
                this.isLoading = false;
            }, error => {
                this.isLoading = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    viewDetail(id) {
        this.vehicleService.getId(id).then(
            success => {
                this.vehicle = success;
                this.listView = false;
                this.showView = true;
                this.createView = false;
                this.editView = false;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    showCreateView() {
        this.listView = false;
        this.showView = false;
        this.createView = true;
        this.editView = false;

        this.alias = '';
        this.plate = '';
        this.type = '';
        this.brand = '';
        this.color = '';
        this.photo = './assets/img/image_empty.jpg';
    }

    upload(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        const url = '/courier/' + randomId;
        const ref = this.storage.ref(url);
        // const task = ref.put(file);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(_url => (this.photo = _url)); }
            )).subscribe();
    }

    uploadNew(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        const url = '/courier/' + randomId;
        const ref = this.storage.ref(url);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(_url => (this.photo = _url)); }
            )).subscribe();
    }

    create() {
        let photoToSave = this.photo;
        if (this.photo === './assets/img/image_empty.jpg') {
            photoToSave = null;
        }
        const vehicle: CourierVehicle = {
            alias: this.alias,
            plate: this.plate,
            type: this.type,
            brand: this.brand,
            color: this.color,
            photo: photoToSave
        };
        this.vehicleService.add(vehicle).then(
            success => {
                this.getAll();
                this.returnList();
                this.photo = '';
            }, (error: any) => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'alias') { title = 'Alias'; }
                            if (title === 'plate') { title = 'Place'; }
                            if (title === 'vehicle') { title = 'Tipo'; }
                            if (title === 'brand') { title = 'Marca'; }
                            if (title === 'type') { title = 'Color'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    showEditView(id) {
        this.photo = './assets/img/image_empty.jpg';

        this.vehicleService.getId(id).then(
            (success: CourierVehicle) => {
                this.vehicle = success;
                this.alias =  this.vehicle.alias;
                this.plate = this.vehicle.plate;
                this.type = this.vehicle.type;
                this.brand = this.vehicle.brand;
                this.color = this.vehicle.color;
                this.idEdit = this.vehicle.id;
                if (this.vehicle.photo !== null) {
                    this.photo = this.vehicle.photo;
                }
                this.listView = false;
                this.showView = false;
                this.createView = false;
                this.editView = true;
            }, error => {
                this.toastr.info(error.message, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    saveEdit() {
        let photoToSave = this.photo;
        if (this.photo === './assets/img/image_empty.jpg') {
            photoToSave = null;
        }
        const vehicle: CourierVehicle = {
            id: this.idEdit,
            alias: this.alias,
            plate: this.plate,
            type: this.type,
            brand: this.brand,
            color: this.color,
            photo: photoToSave
        };
        this.vehicleService.set(vehicle).then(
            success => {
                this.getAll();
                this.returnList();
            }, error => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        console.log('have errors');
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'alias') { title = 'Alias'; }
                            if (title === 'plate') { title = 'Place'; }
                            if (title === 'vehicle') { title = 'Tipo'; }
                            if (title === 'brand') { title = 'Marca'; }
                            if (title === 'type') { title = 'Color'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    deleteVehicle(id) {
        this.vehicleService.delete(id).then(
            success => {
                this.getAll();
                this.returnList();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    pdfDownload() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Vehículos Visitantes', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Placa', 'Vehiculo', 'Marca', 'Color']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 18},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.save('vehiculos.pdf');
    }

    excelDownload() {
        this.excelService.exportAsExcelFile(this.info, 'vehiculos');
    }

    print() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Vehículos Visitantes', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Placa', 'Vehiculo', 'Marca', 'Color']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 18},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.autoPrint();
        window.open(doc.output('bloburl'), '_blank');
    }

    pdfDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Vehículo Visitante', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Placa: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.vehicle.plate, 32, 100);
        doc.setFontType("bold");
        doc.text('Vehículo: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.vehicle.vehicle, 125, 100);

        doc.setFontType("bold");
        doc.text('Marca: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.vehicle.brand, 34, 107);
        doc.setFontType("bold");
        doc.text('Color: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.vehicle.type, 115, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.vehicle.create_date, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.vehicle.update_date, 146, 114);

        if (this.vehicle.photo) {
            this.toDataURL(this.vehicle.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.save('vehiculoDetail.pdf');
            });
        } else {
            doc.save('vehiculoDetail.pdf');
        }

    }

    toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob);
        }));

    printDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Vehículo Visitante', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Placa: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.vehicle.plate, 32, 100);
        doc.setFontType("bold");
        doc.text('Vehículo: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.vehicle.vehicle, 125, 100);

        doc.setFontType("bold");
        doc.text('Marca: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.vehicle.brand, 34, 107);
        doc.setFontType("bold");
        doc.text('Color: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.vehicle.type, 115, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.vehicle.create_date, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.vehicle.update_date, 146, 114);

        if (this.vehicle.photo) {
            this.toDataURL(this.vehicle.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.autoPrint();
                window.open(doc.output('bloburl'), '_blank');
            });
        } else {
            doc.autoPrint();
            window.open(doc.output('bloburl'), '_blank');
        }

    }

    excelDetalle() {
        var excel = [];
        excel = [{'#' : this.vehicle.id, 'Placa': this.vehicle.plate, 'Vehículo':this.vehicle.vehicle, 'Marca':this.vehicle.brand, 'Color':this.vehicle.type, 'Fecha de creación':this.vehicle.create_date, 'Última actualización':this.vehicle.update_date}];
        this.excelService.exportAsExcelFile(excel, 'vehiculDetail');
    }

}
