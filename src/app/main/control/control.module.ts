import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { GuardService } from '../../../model/guard/guard.service';
import { AdminService } from '../../../model/admin/admin.service';
import { VisitaVehiculoService } from '../../../model/visitavehiculo/visitavehiculo.service';
import { VisitanteService } from '../../../model/vistavisitantes/visitantes.service';
import { FuncionarioService } from '../../../model/funcionarios/funcionario.service';
import { IncidenciasService } from '../../../model/incidence/incidence.service';
import { BitacoraService } from '../../../model/bitacora/bitacora.service';
import { VisitasService } from '../../../model/visitas/visitas.service';
import { ConfiguracionService } from '../../../model/configuracion/configuracion.service';
import { BannerService } from '../../../model/banner/banner.service';
import { AlertaService } from '../../../model/alerta/alerta.service';
import { ExcelService } from '../../../model/excel/excel.services';
import { VehistorialService } from '../../../model/historial/vehistorial.service';
import { TabhistoryService } from '../../../model/historial/tabhistory.service';
import { SalidascercoService } from '../../../model/historial/salidacerco.service';

import { HttpClientModule } from '@angular/common/http';
import { ColorPickerModule } from 'ngx-color-picker';
import {NgxPaginationModule} from 'ngx-pagination';
import { ChartsModule } from 'ng2-charts';
import { FusionChartsModule } from 'angular-fusioncharts';
import FusionCharts from 'fusioncharts/core';
import Doughnut2D from 'fusioncharts/viz/doughnut2d';
FusionChartsModule.fcRoot(FusionCharts, Doughnut2D);

import { IgxSnackbarModule } from 'igniteui-angular';
import { ControlComponent } from './control.component';
import {RouterModule, Routes} from '@angular/router';
import { GuardiaComponent } from './guardia/guardia.component';
import { VehiculosComponent } from './visitas/vehiculos/vehiculos.component';
import { VisitantesComponent } from './visitas/visitantes/visitantes.component';
import { FuncionariosComponent } from './visitas/funcionarios/funcionarios.component';
import { VisitasComponent } from './visitas/visitas/visitas.component';
import { VisitasactivasComponent } from './visitas/visitasactivas/visitasactivas.component';
import {VehiculostiposComponent} from './visitas/vehiculostipos/vehiculostipos.component';
import {VehiclestypesService} from '../../../model/vehicletsypes/vehiclestypes.service';

import { FilterPipeModule } from 'ngx-filter-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';

import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import {AsideControlComponent} from './aside/aside.control.component';
import {AdminComponent} from './admin/admin.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { WactivasComponent } from './watches/wactivas/wactivas.component';
import { WtodasComponent } from './watches/wtodas/wtodas.component';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {CercoComponent} from './cerco/cercovirtual/cerco.component';
import { AlertasComponent } from './alertas/alertas.component';
import {CercoService} from '../../../model/cerco/cerco.service';
import {GrupoService} from '../../../model/grupos/grupo.service';
import { PuestoService } from '../../../model/puestos/puestos.service';
import { CercogrupoComponent } from './cerco/cercogrupo/cercogrupo.component';
import { TabletsComponent } from './watches/tablets/tablets.component';
import { PuestosComponent } from './watches/puestos/puestos.component';
import { VehistorialComponent } from './historial/vehistorial/vehistorial.component';
import { TabhistorialComponent } from './historial/tabhistorial/tabhistorial.component';
import { VehicercohistorialComponent } from './historial/vehicercohistorial/vehicercohistorial.component';
import {PopupTablethComponent} from './historial/tabhistorial/popup.tableth.component';
import {PopupVisitComponent} from './visitas/visitas/popup.visit.component';
import {PopupWatchtComponent} from './watches/wtodas/popup.watcht.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AgoTimePipe } from './ago.pipe';
import {VisitPrint} from './visitas/visit.print';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ErrorInterceptor, JwtInterceptor} from '../../_helpers';
import {BusinessService} from '../../../model/business/business.service';
import {EmpresasComponent} from './empresas/empresas.component';
import {DevicesComponent} from './operability/devices/devices.component';
import {OperabilityService} from '../../../model/operability/operability.service';
import {StatisticsComponent} from './operability/statistics/statistics.component';
import {RecordsComponent} from './operability/records/records.component';
import {ProjectComponent} from './tms_project/project.component';
import {PointComponent} from './tms_point/point.component';
import {ContactComponent} from './tms_contact/contact.component';
import {LogisticsComponent} from './tms_logistics/logistics.component';
import {BinnacleComponent} from './tms_binnacle/binnacle.component';
import {FleetComponent} from './tms_vehicles/fleet/fleet.component';
import {VehicleComponent} from './tms_vehicles/vehicle/vehicle.component';
import {LoaderComponent} from './tms_loader/loader.component';
import {PopupHistoryComponent} from './tms_vehicles/fleet/popup.history.component';
import {PopupHhistoryComponent} from './historial/vehistorial/popup.hhistory.component';
import {PopupReportComponent} from './tms_binnacle/popup/popup.report.component';
import {AssignedDriverService} from '../../../model/guard/assigned.driver.service';
import {IncidencesComponent} from './incidences/incidences.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import {OrdersComponent} from './tms_orders/orders.component';
import {AssignedLoaderService} from '../../../model/loader/assigned.loader.service';
import {NumberOnlyDirective} from './NumberOnlyDirective';

export const controlRoutes: Routes = [
    { path: '', component: ControlComponent,
        children: [
            {
                path: '',
                children: [
                    { path: 'proyectos', component: ProjectComponent },
                    { path: 'puntos', component: PointComponent },
                    { path: 'contactos', component: ContactComponent },
                    { path: 'logistica', component: LogisticsComponent },
                    { path: 'proyectos/:id/logistica', component: LogisticsComponent },
                    { path: 'proyectos/:id/bitacora', component: BinnacleComponent },
                    { path: 'vehiculos', component: VehicleComponent },
                    { path: 'flota', component: FleetComponent },
                    { path: 'estibas', component: LoaderComponent },
                    { path: 'admin', component: AdminComponent },
                    { path: 'conductores', component: GuardiaComponent },
                    { path: 'alertas', component: AlertasComponent },
                    { path: 'alertas/:id', component: AlertasComponent },
                    { path: 'visitas/visitas/todas', component: VisitasComponent },
                    { path: 'visitas/visitas/activas', component: VisitasactivasComponent },
                    { path: 'visitas/vehiculos', component: VehiculosComponent },
                    { path: 'visitas/visitantes', component: VisitantesComponent },
                    { path: 'visitas/funcionarios', component: FuncionariosComponent },
                    { path: 'visitas/tipos', component: VehiculostiposComponent },
                    { path: 'vigilancia/activas', component: WactivasComponent },
                    { path: 'vigilancia/todas', component: WtodasComponent },
                    { path: 'vigilancia/tablets', component: TabletsComponent },
                    { path: 'vigilancia/puestos', component: PuestosComponent },
                    { path: 'historial/vehiculos', component: VehistorialComponent },
                    { path: 'historial/tablets', component: TabhistorialComponent },
                    { path: 'historial/vehiculocerco', component: VehicercohistorialComponent },
                    { path: 'empresas', component: EmpresasComponent },
                    { path: 'configuracion', component: ConfiguracionComponent },
                    { path: 'bitacora/tipos', component: IncidencesComponent },
                    { path: 'operatividad/puestos', component: DevicesComponent },
                    { path: 'operatividad/estadisticas', component: StatisticsComponent },
                    { path: 'operatividad/historial', component: RecordsComponent },
                    { path: 'cerco', redirectTo: '/u/control/cerco/virtual', pathMatch: 'full' },
                    { path: 'cerco/virtual', component: CercoComponent },
                    { path: 'cerco/grupos', component: CercogrupoComponent },
                    { path: '', redirectTo: '/u/control/proyectos', pathMatch: 'full' }
                ]
            }
        ]
    }
];

export const controlRouting = RouterModule.forChild(controlRoutes);

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule,
        controlRouting,
        ChartsModule,
        FusionChartsModule,
        FilterPipeModule,
        ColorPickerModule,
        OrderModule,
        Ng2SearchPipeModule,
        LeafletModule.forRoot(),
        LeafletDrawModule.forRoot(),
        LeafletMarkerClusterModule.forRoot(),
        IgxSnackbarModule,
        NgxPaginationModule,
        NgMultiSelectDropDownModule,
        UiSwitchModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        GuardService, AdminService, VisitaVehiculoService, VisitanteService,
        FuncionarioService, ExcelService, PuestoService, VehiclestypesService,
        IncidenciasService, BitacoraService, VisitasService, ConfiguracionService,
        BannerService, AlertaService, CercoService, GrupoService, BusinessService,
        VehistorialService, TabhistoryService, SalidascercoService, VisitPrint,
        OperabilityService, AssignedDriverService, AssignedLoaderService ],
    entryComponents: [ PopupHistoryComponent, PopupHhistoryComponent, PopupTablethComponent, PopupReportComponent,
        PopupVisitComponent, PopupWatchtComponent ],
    declarations: [
        ProjectComponent, PointComponent, ContactComponent, LogisticsComponent, BinnacleComponent, VehicleComponent, FleetComponent,
        GuardiaComponent, AdminComponent, ControlComponent, VehiculostiposComponent, LoaderComponent,
        VehiculosComponent, VisitantesComponent, FuncionariosComponent, IncidencesComponent, VisitasComponent, VisitasactivasComponent,
        OrdersComponent,
        AsideControlComponent,
        ConfiguracionComponent,
        WactivasComponent,
        WtodasComponent,
        DevicesComponent,
        StatisticsComponent,
        CercoComponent,
        VehicercohistorialComponent,
        AlertasComponent,
        CercogrupoComponent,
        RecordsComponent,
        TabletsComponent,
        PuestosComponent,
        EmpresasComponent,
        VehistorialComponent,
        TabhistorialComponent,
        PopupHistoryComponent,
        PopupHhistoryComponent,
        PopupTablethComponent,
        PopupReportComponent,
        PopupVisitComponent,
        PopupWatchtComponent,
        IncidencesComponent,
        AgoTimePipe,
        NumberOnlyDirective],
    bootstrap: [ ControlComponent ]
})
export class ControlModule {}
