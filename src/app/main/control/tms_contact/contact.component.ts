import { Component } from '@angular/core';
import 'jspdf-autotable';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../environments/environment';
import {Contact} from '../../../../model/contact/contact';
import {ContactService} from '../../../../model/contact/contact.service';
import {AuthenticationService} from '../../../_services';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})
export class ContactComponent {
    // General
    data: Contact[] = [];
    isLoading = false;

    filter: string;
    numElement = 10;
    p;

    key = 'name'; // set default
    reverse = false;

    newName: any;
    newDni: any;
    newPhone: any;

    editObject: any;
    editName: any;
    editDni: any;
    editPhone: any;

    deleteObject: any;
    deleteName: any;



    constructor(private authService: AuthenticationService,
                private contactService: ContactService,
                private toastr: ToastrService) {
        this.getAll();
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    getAll() {
        this.isLoading = true;
        this.data = [];
        this.contactService.getAll().then(
            (success: any) => {
                console.log(success);
                this.data = success.data;
                this.isLoading = false;
            }, error => {
                this.isLoading = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    editModal(contact) {
        this.editObject = contact;
        this.editName = contact.name;
        this.editDni = contact.dni;
        this.editPhone = contact.phone;
    }

    deleteModal(contact) {
        this.deleteObject = contact;
        this.deleteName = contact.name;
    }

    create() {
        const newObject: Contact = {
            name: this.newName,
            dni: this.newDni,
            phone: this.newPhone,
            registered_by: this.authService.getUser().id,
            company_id: this.authService.getUser().company_id,
        };
        this.contactService.add(newObject).then(
            (success: any) => {
                this.newName = '';
                this.newDni = '';
                this.newPhone = '';
                const contact: any = success.result;
                this.data.push(contact);
                this.toastr.success('El contacto ' + contact.name
                    + ' ha sido creado con exito!.', '',
                    { positionClass: 'toast-bottom-center'});
            }, error => {
                if (error.status === 422 && error.error && error.error.message) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    edit() {
        const updateObject: Contact = {
            id: this.editObject.id,
            name: this.editName,
            dni: this.editDni,
            phone: this.editPhone
        };
        this.contactService.set(updateObject).then(
            (success: any) => {
                this.toastr.success('El contacto ' + success.result.name
                    + ' ha sido actualizado.', '',
                    { positionClass: 'toast-bottom-center'});
                this.editObject.name = success.result.name;
                this.editObject.dni = success.result.dni;
                this.editObject.phone = success.result.phone;
                this.editObject.update_at = success.result.update_at;
            }, error => {
                if (error.status === 422 && error.error && error.error.message) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    delete() {
        this.contactService.delete(this.deleteObject.id).then(
            success => {
                this.toastr.success('Contacto ' + this.deleteObject.name
                    + ' ha sido Eliminado!', '',
                    { positionClass: 'toast-bottom-center'});
                this.getAll();
            }, error => {
                if (error.status === 422) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }
}
