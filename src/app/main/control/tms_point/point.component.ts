import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireStorage } from 'angularfire2/storage';

import * as L from 'leaflet';
import 'leaflet.markercluster';
import 'leaflet-draw';
import {GlobalOsm} from '../../../global.osm';
import {PointService} from '../../../../model/point/point.service';
import {ToastrService} from 'ngx-toastr';
import {Point} from '../../../../model/point/point';
import {RegionsService} from '../../../../model/regions/regions.service';
import {City} from '../../../../model/regions/city';
import {Region} from '../../../../model/regions/region';
import {environment} from '../../../../environments/environment';
import {AuthenticationService} from '../../../_services';
import {e} from '@angular/core/src/render3';

@Component({
    selector: 'app-point',
    templateUrl: './point.component.html',
    styleUrls: ['./point.component.css']
})

export class PointComponent implements OnInit {
    // General
    data: Point[] = [];
    isLoading = false;

    dataFilter;
    numElement = 10;
    p;

    regions: Region[] = [];
    cities: City[] = [];

    selectedName;
    selectedAddress;
    selectedRegion;
    selectedCity;
    selectedDate;
    selectedTime;
    latitude;
    longitude;
    selectedComment;

    listView = true;
    createView = false;
    editView = false;
    showView = false;

    showObject: Point = {};
    editObject: Point = {};
    deleteObject: Point = {};

    key = 'id';
    reverse = false;

    map: L.Map;
    zoom: number;
    center = L.latLng(([ -2.071522, -79.607105 ]));
    layersControlOptions;
    baseLayers;
    options;
    marker;

    constructor (
        private authService: AuthenticationService,
        public router: Router,
        private storage: AngularFireStorage,
        private pointService: PointService,
        private regionsService: RegionsService,
        private toastr: ToastrService,
        private globalOSM: GlobalOsm) {
        /* Map default options */
        this.layersControlOptions = this.globalOSM.layersOptions;
        this.baseLayers = this.globalOSM.baseLayers;
        this.options = this.globalOSM.defaultOptions;
        this.zoom = this.globalOSM.zoom;
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    ngOnInit() {
        this.returnList();
        this.getAllPoints();
    }

    returnList() {
        this.listView = true;
        this.showView = false;
        this.createView = false;
        this.editView = false;

        this.selectedName = '';
        this.selectedRegion = '';
        this.selectedCity = '';
        this.selectedAddress = '';
        this.latitude = '';
        this.longitude = '';
        this.selectedComment = '';
        this.selectedDate = '';
        this.selectedTime = '';

        if (this.map !== undefined) {
            this.map.remove();
        }
    }

    showLoading() {
        this.isLoading = true;
        this.data = [];
    }

    dismissLoading() {
        this.isLoading = false;
    }

    getAllPoints() {
        this.showLoading();
        this.pointService.getAll().then(
            (success: any) => {
                this.dismissLoading();
                this.data = success.data;
            }, error => {
                this.dismissLoading();
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    showCreateView() {
        this.createView = true;
        this.listView = false;
        this.showView = false;
        this.editView = false;

        this.latitude = this.globalOSM.center.lat;
        this.longitude = this.globalOSM.center.lng;
        this.regionsService.getAllRegions().then(
            (success: any) => {
                this.regions = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
        this.regionsService.getAllCities().then(
            (success: any) => {
                this.cities = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    saveCreate() {
        if (!this.selectedName) {
            this.toastr.info('Debe escribir un nombre para la ubicación.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.selectedRegion) {
            this.toastr.info('Debe seleccionar una region.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.selectedCity) {
            this.toastr.info('Debe seleccionar una ciudad.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.selectedAddress) {
            this.toastr.info('Debe escribir la dirección.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.latitude || !this.longitude) {
            this.toastr.info('Debe seleccionar un punto en el mapa.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else {
            let route: Point = {
                name: this.selectedName,
                province: this.selectedRegion,
                city: this.selectedCity,
                address: this.selectedAddress,
                latitude: this.latitude,
                longitude: this.longitude,
                comment: this.selectedComment,
                registered_by: this.authService.getUser().id,
                company_id: this.authService.getUser().company_id,
            };
            this.pointService.add(route).then(
                (success: any) => {
                    route = success.result;
                    this.data.push(route);
                    this.toastr.success('La ubicación ' + route.name
                        + ' ha sido creado con exito!.', '',
                        { positionClass: 'toast-bottom-center'});
                    this.returnList();
                }, error => {
                    if (error.status === 422 && error.error && error.error.message) {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    } else {
                        this.toastr.info(environment.ERROR_GENERAL, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                }
            );
        }
    }

    showEditView(route) {
        this.editObject = route;
        this.createView = false;
        this.listView = false;
        this.showView = false;
        this.editView = true;

        this.regionsService.getAllRegions().then(
            (success: any) => {
                this.regions = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
        this.regionsService.getAllCities().then(
            (success: any) => {
                this.cities = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    saveEdit() {
        if (!this.editObject.name) {
            this.toastr.info('Debe escribir un nombre para la ubicación.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.editObject.province) {
            this.toastr.info('Debe seleccionar una region.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.editObject.city) {
            this.toastr.info('Debe seleccionar una ciudad.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.editObject.address) {
            this.toastr.info('Debe escribir la dirección.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (!this.editObject.latitude || !this.editObject.longitude) {
            this.toastr.info('Debe seleccionar un punto en el mapa.', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else {
            this.pointService.set(this.editObject).then(
                (success: any) => {
                    this.editObject.update_at = success.result.update_at;
                    this.toastr.success('La ubicación ' + this.editObject.name
                        + ' ha sido actualizada con exito!.', '',
                        { positionClass: 'toast-bottom-center'});
                    this.returnList();
                }, error => {
                    if (error.status === 422 && error.error && error.error.message) {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    } else {
                        this.toastr.info(environment.ERROR_GENERAL, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                }
            );
        }
    }

    showDetailsView(route) {
        this.showObject = route;
        this.createView = false;
        this.listView = false;
        this.showView = true;
        this.editView = false;
    }

    deleteModal(route) {
        this.deleteObject = route;
    }

    delete() {
        this.pointService.delete(this.deleteObject.id).then(
            success => {
                this.toastr.success('La ubicación ' + this.deleteObject.name
                    + ' ha sido Eliminado!', '',
                    { positionClass: 'toast-bottom-center'});
                this.getAllPoints();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    onMapReady(map: L.Map) {
        this.map = map;
        this.globalOSM.setupLayer(this.map);
        /***************** On Create view *****************/
        if (this.createView) {
            this.marker = new L.Marker(
                this.globalOSM.center, {draggable: true}
            ).addTo(map);
            this.center = this.globalOSM.center;
            this.zoom = this.globalOSM.zoom;
            this.marker.on('drag', e => {
                const markerP = e.target;
                const position = markerP.getLatLng();
                this.latitude = position.lat;
                this.longitude = position.lng;
            });
        }
        /***************** On Show view *****************/
        if (this.showView) {
            const latLng =  L.latLng(Number(this.showObject.latitude), Number(this.showObject.longitude));
            this.marker = new L.Marker(
                latLng, {draggable: false}
            ).addTo(map);
            this.center = latLng;
            this.zoom = this.globalOSM.fullZoom;
        }
        /***************** On Edit view *****************/
        if (this.editView) {
            const latLng =  L.latLng(Number(this.editObject.latitude), Number(this.editObject.longitude));
            this.marker = new L.Marker(
                latLng, {draggable: true}
            ).addTo(map);
            this.center = latLng;
            this.zoom = this.globalOSM.fullZoom;
            this.marker.on('drag', e => {
                const markerP = e.target;
                const position = markerP.getLatLng();
                this.editObject.latitude = position.lat;
                this.editObject.longitude = position.lng;
            });
        }
    }

    moveMarket(latitude, longitude) {
        try {
            const latLng = L.latLng(Number(latitude), Number(longitude));
            this.marker.setLatLng(latLng);
            this.center = latLng;
        } catch (e) { console.log(e) }
    }
}
