import { Component } from '@angular/core';
import { Router } from '@angular/router';
import 'jspdf-autotable';
import {ToastrService} from 'ngx-toastr';
import {ProjectService} from '../../../../model/proyect/project.service';
import {Project} from '../../../../model/proyect/project';
import {environment} from '../../../../environments/environment';
import {AuthenticationService} from '../../../_services';

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css']
})
export class ProjectComponent {
    // General
    data: Project[] = [];
    isLoading = false;

    filter: string;
    numElement = 10;
    p;

    key = 'name'; // set default
    reverse = false;

    newName: any;
    newDescription: any;

    editObject: any;
    editName: any;
    editDescription: any;

    deleteObject: any;
    deleteName: any;

    constructor(private authService: AuthenticationService,
                public router: Router,
                private projectService: ProjectService,
                private toastr: ToastrService) {
        this.getAll();
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    getAll() {
        this.isLoading = true;
        this.data = [];
        this.projectService.getAll().then(
            (success: any) => {
                this.data = success.data;
                this.isLoading = false;
            }, error => {
                this.isLoading = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    editModal(project) {
        this.editObject = project;
        this.editName = project.name;
        this.editDescription = project.description;
    }

    deleteModal(project) {
        this.deleteObject = project;
        this.deleteName = project.name;
    }

    create() {
        const newObject: Project = {
            registered_by: this.authService.getUser().id,
            company_id: this.authService.getUser().company_id,
            name: this.newName,
            description: this.newDescription,
            percentage: 0
        };
        this.projectService.add(newObject).then(
            (success: any) => {
                this.newName = '';
                this.newDescription = '';
                const project: any = success.result;
                this.data.push(project);
                this.toastr.success('El proyecto ' + project.name
                    + ' ha sido creado con exito!.', '',
                    { positionClass: 'toast-bottom-center'});
            }, error => {
                if (error.status === 422 && error.error && error.error.message) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    edit() {
        const updateObject: Project = {
            id: this.editObject.id,
            name: this.editName,
            description: this.editDescription
        };
        this.projectService.set(updateObject).then(
            (success: any) => {
                this.toastr.success('El proyecto ' + success.result.name
                    + ' ha sido actualizado.', '',
                    { positionClass: 'toast-bottom-center'});
                this.editObject.name = success.result.name;
                this.editObject.description = success.result.description;
            }, error => {
                if (error.status === 422 && error.error && error.error.message) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    delete() {
        this.projectService.delete(this.deleteObject.id).then(
            success => {
                this.toastr.success('Proyecto ' + this.deleteObject.name
                    + ' ha sido Eliminado!', '',
                    { positionClass: 'toast-bottom-center'});
                this.getAll();
            }, error => {
                if (error.status === 422) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    showDetailsView(project) {
        this.authService.setProject(project);
        this.router.navigate(['/u/control/proyectos/' + project.id + '/logistica']).then();
    }

    showBinnacle(project) {
        this.authService.setProject(project);
        this.router.navigate(['/u/control/proyectos/' + project.id + '/bitacora']).then();
    }

    convertTime(time: string) {
        const res = time.substring(0, 2);
        let resNumber = Number(res);
        if (resNumber > 12) {
            resNumber = resNumber - 12;
            time = String(resNumber) + time.substring(2, time.length) + " PM";
        } else {
            time = time + " AM"
        }
        return time;
    }
}
