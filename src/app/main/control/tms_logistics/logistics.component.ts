import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import 'leaflet.markercluster';
import 'leaflet-draw';
import {PointService} from '../../../../model/point/point.service';
import {ToastrService} from 'ngx-toastr';
import {RegionsService} from '../../../../model/regions/regions.service';
import {City} from '../../../../model/regions/city';
import {Region} from '../../../../model/regions/region';
import {environment} from '../../../../environments/environment';
import {ProjectService} from '../../../../model/proyect/project.service';
import {Project} from '../../../../model/proyect/project';
import {Order} from '../../../../model/order/order';
import {OrderService} from '../../../../model/order/order.service';
import {Contact} from '../../../../model/contact/contact';
import {ContactService} from '../../../../model/contact/contact.service';
import {Point} from '../../../../model/point/point';
import {Route} from '../../../../model/route/route';
import {RouteService} from '../../../../model/route/route.service';
import {OrderContact} from '../../../../model/contact/order.contact';
import {OrderContactService} from '../../../../model/contact/order.contact.service';
import {AuthenticationService} from '../../../_services';
import {AssignedDriver} from '../../../../model/guard/assigned.driver';
import {GuardService} from '../../../../model/guard/guard.service';
import {Guard} from '../../../../model/guard/guard';
import {AssignedDriverService} from '../../../../model/guard/assigned.driver.service';
import {LoaderService} from '../../../../model/loader/loader.service';
import {Loader} from '../../../../model/loader/loader';
import {AssignedLoader} from '../../../../model/loader/assigned.loader';
import {AssignedLoaderService} from '../../../../model/loader/assigned.loader.service';

@Component({
    selector: 'app-logistics',
    templateUrl: './logistics.component.html',
    styleUrls: ['./logistics.component.css']
})

export class LogisticsComponent implements OnInit {
    // General
    projectId;
    project: Project = {};
    routes: Route[] = [];
    drivers: Guard[] = [];
    assignedDrivers: AssignedDriver[] = [];
    assignedLoaders: AssignedLoader[] = [];
    orderContacts: OrderContact[] = [];
    order: Order = {
        loaders: 0,
        routers: 1,
        type_dispatch: false,
        type_withdrawal: false,
        type_movements: false,

        access_terrace: false,
        access_hill: false,
        access_river: false,
        access_restriction: false,
        access_load500: false,
        access_narrow: false,

        truck_350kg: false,
        truck_550kg: false,
        truck_900kg: false,
        truck_1tn: false,
        truck_2tn: false,
        truck_5tn: false,
        truck_7tn: false,
        truck_12tn: false,
        truck_18tn: false,
        truck_low: false,

        additional_container20: false,
        additional_container40: false,
        additional_trailer40: false,
        additional_trailer48: false,
        additional_trailer53: false,
        additional_platform8: false,
        additional_platform12: false,
        additional_forklift_1tn: false,
        additional_forklift_2tn: false,
        additional_forklift_5tn: false,
        additional_truck_4x4: false,
        additional_barge: false,
        additional_crane_25tn: false,
        additional_packer: false
    };

    regions: Region[] = [];
    cities: City[] = [];

    contacts: Contact[] = [];
    points: Point[] = [];
    loaders: Loader[] = [];

    origins = 0;
    destinations = 0;

    filterValue: string;

    routeSelected: Route = {};
    timeSelected: '';

    modalOrigin = false;

    containsOrder = false;

    contactSelected: OrderContact = {};

    @ViewChild('closeModal') private closeModal: ElementRef;

    @ViewChild('closeModalType') private closeModalType: ElementRef;

    deleteDriverId: number;
    deleteDriverName: string;

    deleteLoaderId: number;
    deleteLoaderName: string;

    deleteRouteId: number;
    deleteRouteName: string;

    constructor (
        private orderContactService: OrderContactService,
        private pointService: PointService,
        private contactService: ContactService,
        private loaderService: LoaderService,
        private assignedLoaderService: AssignedLoaderService,
        private orderService: OrderService,
        private projectService: ProjectService,
        private route: ActivatedRoute,
        private routeService: RouteService,
        private regionsService: RegionsService,
        private authService: AuthenticationService,
        private driverService: GuardService,
        private assignDriverService: AssignedDriverService,
        private toastr: ToastrService) {
    }

    ngOnInit() {
        this.project = this.authService.getProject();
        this.route.url.subscribe(value => {
            const projectId = value[value.length - 2].path;
            if (Number(projectId)) {
                this.projectId = projectId;
                this.getRegions();
            }
        });
    }

    getRegions() {
        this.regionsService.getAllRegions().then(
            (success: any) => {
                this.regions = success.data;
                this.getCities();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getCities() {
        this.regionsService.getAllCities().then(
            (success: any) => {
                this.cities = success.data;
                this.getProject();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getProject() {
        this.projectService.getId(this.projectId).then(
            (success: Project) => {
                this.project = success;
                this.getOrder();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getOrder() {
        this.containsOrder = false;
        this.orderService.getByProject(this.projectId)
            .then(
            (success: Order) => {
                this.order = success;
                this.getOrderRoutes();
                this.containsOrder = true;
            }, error => {
                if (error.status !== 404) {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.getOrderRoutes();
                }
            }
        );
    }

    getOrderRoutes(loadMore = true) {
        this.origins = 0;
        this.destinations = 0;
        this.routeService.getByProject(this.projectId).then(
            (success: any) => {
                this.routes = success.data;
                this.routes.forEach(route => {
                    if (route.type === 'ORIGIN') {
                        this.origins++;
                    }
                    if (route.type === 'DESTINATION') {
                        this.destinations++;
                    }
                });
                if (loadMore) {
                    this.getOrderContacts();
                }
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getOrderContacts() {
        this.orderContactService.getByProject(this.projectId).then(
            (success: any) => {
                this.orderContacts = success.data;
                this.getAssignedDrivers();
                this.getAssignedLoaders();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    saveCreate() {
        if (this.order.loaders == null || this.order.loaders < 0) {
            this.toastr.info('El numero de estibas debe ser mayor o igual a 0.', 'Error',
                {positionClass: 'toast-bottom-center'});
        } else if (!this.order.routers || this.order.routers < 1) {
            this.toastr.info('El numero de rutas debe ser mayor o igual a 1.', 'Error',
                {positionClass: 'toast-bottom-center'});
        } else {
            this.order.project_id = this.project.id;
            this.orderService.add(this.order).then(
                (success: any) => {
                    this.order = success.result;
                    this.toastr.success('El projecto ' + this.project.name
                        + ' ha sido actualizado con exito!.', '',
                        { positionClass: 'toast-bottom-center'});
                    this.containsOrder = true;
                }, error => {
                    if (error.status === 422 && error.error && error.error.message) {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    } else {
                        this.toastr.info(environment.ERROR_GENERAL, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                }
            );
        }
    }

    getSites(origin: boolean) {
        this.filterValue = '';
        this.modalOrigin = origin;
        this.pointService.getAll().then(
            (success: any) => {
                this.points = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getDrivers() {
        this.filterValue = '';
        this.drivers = [];
        this.driverService.getAll().then(
            (success: any) => {
                success.data.forEach(driver => {
                    if (driver.vehicle_plate) {
                        this.drivers.push(driver);
                    }
                });
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    selectSite(site: Point) {
        this.routeSelected = {
            project_id: this.project.id,
            name: site.name,
            province: site.province,
            city: site.city,
            address: site.address,
            latitude: site.latitude,
            longitude: site.longitude,
            type: this.modalOrigin ? 'ORIGIN' : 'DESTINATION',
        };
    }

    saveDriver(driver: Guard) {
        const plate = driver.vehicle_plate;
        const alias = driver.vehicle_plate ? (driver.claro_vehicle ? driver.claro_vehicle.alias : driver.tms_vehicle.alias) : "";
        const assigned: AssignedDriver = {
            project_id: this.projectId,
            driver_id: driver.id,
            order_id: this.order.id,
            vehicle_plate: plate,
            vehicle_alias: alias
        };
        this.assignDriverService.add(assigned).then(
            (success: any) => {
                this.toastr.success('El conductor ' + driver.name
                    + ' ha sido agregada con exito!.', '',
                    { positionClass: 'toast-bottom-center'});
                this.assignedDrivers = [];
                this.getAssignedDrivers();
            }, error => {
                if (error.status === 422 && error.error && error.error.message) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    saveLoader(loader: Loader) {
        const assigned: AssignedLoader = {
            project_id: this.projectId,
            loader_id: loader.id,
            order_id: this.order.id,
        };
        this.assignedLoaderService.add(assigned).then(
            (success: any) => {
                this.toastr.success('El estiba ' + loader.name
                    + ' ha sido agregada con exito!.', '',
                    { positionClass: 'toast-bottom-center'});
                this.assignedLoaders = [];
                this.getAssignedLoaders();
            }, error => {
                if (error.status === 422 && error.error && error.error.message) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    getAssignedDrivers() {
        this.assignDriverService.getByProjectId(this.projectId).then(
            (success: any) => {
                this.assignedDrivers = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getAssignedLoaders() {
        this.assignedLoaderService.getByProjectId(this.projectId).then(
            (success: any) => {
                this.assignedLoaders = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    confirmDeleteDriver(assignedDriver: AssignedDriver) {
        this.deleteDriverName = assignedDriver.driver.name + ' ' + assignedDriver.driver.lastname;
        this.deleteDriverId = assignedDriver.id;
    }

    confirmDeleteLoader(assignedLoader: AssignedLoader) {
        this.deleteLoaderName = assignedLoader.loader.name + ' ' + assignedLoader.loader.lastname;
        this.deleteLoaderId = assignedLoader.id;
    }

    confirmDeleteRoute(route: Route) {
        this.deleteRouteName = route.name;
        this.deleteRouteId = route.id;
    }

    deleteRoute() {
        this.routeService.delete(this.deleteRouteId).then(
            (success: any) => {
                this.toastr.success('La ruta ha sido Eliminado!', '',
                    { positionClass: 'toast-bottom-center'});
                this.getOrderRoutes(false);
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    deleteDriver() {
        this.assignDriverService.delete(this.deleteDriverId).then(
            (success: any) => {
                this.toastr.success('El conductor ha sido Eliminado!', '',
                    { positionClass: 'toast-bottom-center'});
                this.getAssignedDrivers();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    deleteLoader() {
        this.assignedLoaderService.delete(this.deleteLoaderId).then(
            (success: any) => {
                this.toastr.success('El estiba ha sido Eliminado!', '',
                    { positionClass: 'toast-bottom-center'});
                this.getAssignedLoaders();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getContacts() {
        this.filterValue = '';
        this.contactService.getAll().then(
            (success: any) => {
                this.contacts = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getLoaders() {
        this.filterValue = '';
        this.loaderService.getAll().then(
            (success: any) => {
                this.loaders = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    saveRoute() {
        if (!this.routeSelected.date || !this.timeSelected) {
            this.toastr.info('Debe agregar la fecha y hora.', 'Error',
                { positionClass: 'toast-center-center'});
        } else if (this.routeSelected.bulls == null || this.routeSelected.bulls < 0) {
            this.toastr.info('La cantidad de bultos debe ser mayor o igual a 0.', 'Error',
                { positionClass: 'toast-center-center'});
        } else {
            this.routeSelected.time = this.timeSelected + ':00';
            this.routeService.add(this.routeSelected).then(
                (success: any) => {
                    this.closeModal.nativeElement.click();
                    this.toastr.success('La ruta ' + this.project.name
                        + ' ha sido agregada con exito!.', '',
                        { positionClass: 'toast-bottom-center'});
                    this.getOrderRoutes();
                }, error => {
                    if (error.status === 422 && error.error && error.error.message) {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    } else {
                        this.toastr.info(environment.ERROR_GENERAL, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                }
            );
        }
    }

    selectContact(contact) {
        this.contactSelected = {
            project_id: this.project.id,
            name: contact.name,
            dni: contact.dni,
            phone: contact.phone
        };
    }

    saveContact() {
        if (!this.contactSelected.type) {
            this.toastr.info('Debe agregar el tipo de contacto.', 'Error',
                { positionClass: 'toast-center-center'});
        } else {
            this.orderContactService.add(this.contactSelected).then(
                (success: any) => {
                    this.closeModalType.nativeElement.click();
                    this.toastr.success('El contacto ' + this.contactSelected.name
                        + ' ha sido agregado con exito!.', '',
                        { positionClass: 'toast-bottom-center'});
                    this.getOrderContacts();
                }, error => {
                    if (error.status === 422 && error.error && error.error.message) {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    } else {
                        this.toastr.info(environment.ERROR_GENERAL, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                }
            );
        }
    }
}
