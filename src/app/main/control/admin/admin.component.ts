import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../../../../model/admin/admin.service';
import { Admin } from '../../../../model/admin/admin';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ExcelService } from '../../../../model/excel/excel.services';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../environments/environment';
import { UserService } from '../../../_services/firebaseRoleServices/connectionServices/user.service';
import { User } from '../../../_services/firebaseRoleServices/fireModels/user';
import { AppConst } from '../../../shared/app.const';
@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})

export class AdminComponent {
    // General
    data: Admin[] = [];
    isLoading = false;

    admin: Admin = {};
    error: string;
    // views admin
    listView: boolean;
    detailsView: boolean;
    createView: boolean;
    editView: boolean;
    // edit
    nombre:string;
    apellido:string;
    correo:string;
    identificacion:string;
    photo:string;
    contrasena:string = "password";
    notifications = false;
    idEdit:number;
    errorEdit:boolean = false;
    errorEditData:boolean = false;
    errorEditMsg:string;
    //createBoundView
    namea:string;
    lastnamea:string;
    emaila:string;
    notificationsa: boolean = false;
    dnia:string;
    passworda:string;
    photoa:string;
    errorSave:boolean = false;
    errorSaveData:boolean = false;
    errorNewMsg:string;
    //eliminar
    errorDelete:boolean = false;
    errorDeleteData:boolean = false;
    filter:string;
    //imagen firebase
    uploadPercent: Observable<number>;
    downloadURL: Observable<string>;
    p: number = 1;
    numElement:number = 10;
    //exportaciones
    contpdf:any = [];
    info: any = [];

    public  permissions = [
        { enabled: false,    see: false,   edit: false, name: 'Proyectos', number:0},
        { enabled: false,    see: false,   edit: false, name: 'Ubicaciones', number:0},
        { enabled: false,    see: false,   edit: false, name: 'Administradores', number: 1},
        { enabled: false,    see: false,   edit: false, name: 'Conductores', number:2},
        { enabled: false,    see: false,   edit: false, name: 'Estibas', number: 3},
        { enabled: false,    see: false,   edit: false, name: 'Vehículos', number: 4},
        { enabled: false,    see: false,   edit: false, name: 'Vehículos Flota', number: 5},
        { enabled: false,    see: false,   edit: false, name: 'Incidencias', number: 6},
        { enabled: false,    see: false,   edit: false, name: 'Configuracion', number: 7},
        { enabled: false,    see: false,   edit: false, name: 'Contactos', number: 6},
        { enabled: false,    see: false,   edit: false, name: 'Historial de Ubicaciones', number: 7}
    ];
    public role = [
        { value: 'MASTER', title: 'Principal'},
        { value: 'ADMIN', title: 'Administrador'},
        { value: 'COMPANY', title: 'Empresa'},
        { value: 'BASIC', title: 'Usuario'}
    ];
    public selectedRole = '';
    public panel = false;
    public roleAdmin= '';
    key: string = 'id'; // set default
    reverse = true;

    constructor(
        public router: Router,
        private adminService: AdminService,
        private storage: AngularFireStorage,
        private excelService: ExcelService,
        private toastr: ToastrService,
        public userService: UserService) {
        this.getAll();
        this.listView = true;
        this.detailsView = false;
        this.createView = false;
        this.editView = false;
        this.roleAdmin = localStorage.getItem('ROLE');
        this.filterRole(this.roleAdmin);

    }

   

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    getAll() {
        this.isLoading = true;
        this.data = [];
        this.adminService.getAll().then(
            (success: any) => {
                this.data = success.data;
                this.isLoading = false;
                var body = [];
                var excel = [];
                for (var i = 0; i < this.data.length; i++) {
                    // this.data[i].id = Number(this.data[i].id);
                    // this.data[i].dni = Number(this.data[i].dni);
                    excel.push({'#' : this.data[i].id, 'Cedula': this.data[i].dni, 'Nombre': this.data[i].name,
                        'Apellido': this.data[i].lastname, 'Correo': this.data[i].email});
                    body.push([this.data[i].id, this.data[i].dni, this.data[i].name, this.data[i].lastname, this.data[i].email]);
                }
                this.contpdf = body;
                this.info = excel;
            }, error => {
                this.isLoading = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    viewDetail(id) {
        this.adminService.getId(id).then(
            success => {
                this.admin = success;
                this.listView = false;
                this.detailsView = true;
                this.createView = false;
                this.editView = false;
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    // on general error
                }
            }
        );
    }

    upload(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        const url = '/icsse/' + randomId;
        const ref = this.storage.ref(url);
        // const task = ref.put(file);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(next => (this.photo = next)); }
            )).subscribe();
    }

    uploadNew(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        const url = '/icsse/' + randomId;
        const ref = this.storage.ref(url);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(next => (this.photoa = next)); }
            )).subscribe();
    }

    return() {
        this.listView = true;
        this.detailsView = false;
        this.createView = false;
        this.editView = false;
        this.errorEditData = false;
    }

    editAdmin(id) {
        this.adminService.getId(id).then(
            success => {
                this.admin = success;
                console.log(this.admin);
                this.nombre = this.admin.name;
                this.apellido = this.admin.lastname;
                this.correo = this.admin.email;
                if (this.admin.photo == null) {
                    this.photo = './assets/img/user_empty.jpg';
                } else {
                    this.photo = this.admin.photo;
                }
                this.identificacion = this.admin.dni;
                this.notifications = this.admin.notifications;
                this.idEdit = this.admin.id;
                this.listView = false;
                this.detailsView = false;
                this.createView = false;
                this.editView = true;
            }, error => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'name') { title = 'Nombre'; }
                            if (title === 'lastname') { title = 'Apellido'; }
                            if (title === 'dni') { title = 'Cedula'; }
                            if (title === 'email') { title = 'Correo'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                    this.errorSaveData = true;
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                    this.errorSave = true;
                }
            }
        );
    }

    getValueEdit() {
        if (this.photo === './assets/img/user_empty.jpg') {
            this.photo = null;
        }
        if (this.contrasena === 'password') {
            return {
                id: this.idEdit,
                dni: this.identificacion,
                name: this.nombre,
                lastname: this.apellido,
                email: this.correo,
                photo: this.photo,
                notifications: this.notifications
            };
        } else {
            return {
                id: this.idEdit,
                dni: this.identificacion,
                name: this.nombre,
                lastname: this.apellido,
                email: this.correo,
                password: this.contrasena,
                photo: this.photo,
                notifications: this.notifications
            };
        }
    }

    saveEdit() {
        const values = this.getValueEdit();
        this.adminService.set(values).then(
            success => {
                this.getAll();
                this.return();
                this.photo = '';
                this.errorEditData = false;
                this.errorEdit = false;
            }, error => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'name') { title = 'Nombre'; }
                            if (title === 'lastname') { title = 'Apellido'; }
                            if (title === 'dni') { title = 'Cedula'; }
                            if (title === 'email') { title = 'Correo'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                    this.errorSaveData = true;
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                    this.errorSave = true;
                }
            }
        );
    }

    createAdmin() {
        this.listView = false;
        this.detailsView = false;
        this.createView = true;
        this.editView = false;
        this.photoa = './assets/img/user_empty.jpg';
    }

    saveNewAdmin() {
        if (this.photoa === './assets/img/user_empty.jpg'){
            this.photoa = null;
        }
        let rol;
        let admin = JSON.parse(localStorage.getItem('USER-ADMIN'));
        const createAdmin: Admin = {
            dni: this.dnia,
            name: this.namea,
            lastname: this.lastnamea,
            email: this.emaila,
            password: this.passworda,
            photo: this.photoa,
            registered_by: admin.id,
            company_id: 1,
            rol: AppConst.USER.COMPANY,
            notifications: this.notificationsa
        };

        for(let i = 0; i < this.role.length; i++ ) {
            if (this.role[i].title === this.selectedRole){
                rol = this.role[i].value;
                break;
            }
        }

        this.adminService.add(createAdmin).then(
            success => {
                const createRol: User ={
                    dni: this.dnia,
                    name: this.namea,
                    lastname: this.lastnamea,
                    rol: rol,
                    permissions: this.permissions,
                    registered_by: admin.id,
                    company_id: 1,
                };
                // this.userService.addUser(createRol);
                this.getAll();
                this.return();
                this.dnia = '';
                this.namea = '';
                this.lastnamea = '';
                this.emaila = '';
                this.photoa = '';
                this.errorSave = false;
                this.errorSaveData = false;

            }, error => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'name') { title = 'Nombre'; }
                            if (title === 'lastname') { title = 'Apellido'; }
                            if (title === 'dni') { title = 'Cedula'; }
                            if (title === 'email') { title = 'Correo'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                    this.errorSaveData = true;
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                    this.errorSave = true;
                }
            }
        );
    }

    deleteAdmin(id) {
        this.adminService.delete(id).then(
            success => {
                this.getAll();
                this.return();
                this.errorDeleteData = false;
                this.errorDelete = false;
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                    this.errorDeleteData = true;
                } else {
                    // on general error
                    this.errorDelete = true;
                }
            }
        );
    }

    activateAdmin(id) {
        this.adminService.activeAdmin(id).then(
            success => {
                this.getAll();
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    // on general error
                }
            }
        );
    }

    disableAdmin(id) {
        this.adminService.desactiveAdmin(id).then(
            success => {
                this.getAll();
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    // on general error
                }
            }
        );
    }

    pdfDownload() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuarios Administradores del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Cédula', 'Nombre', 'Apellido', 'Correo']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 10},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.save('adminstradores.pdf');
    }

    excelDownload() {
        this.excelService.exportAsExcelFile(this.info, 'admin');
    }

    print() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuarios Administradores del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Cédula', 'Nombre', 'Apellido', 'Correo']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 10},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.autoPrint();
        window.open(doc.output('bloburl'), '_blank');
    }

    pdfDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuario Administrador del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Nombre: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.admin.name, 34, 100);
        doc.setFontType("bold");
        doc.text('Apellido: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.admin.lastname, 123, 100);

        doc.setFontType("bold");
        doc.text('Cédula: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.admin.dni, 34, 107);
        doc.setFontType("bold");
        doc.text('Correo: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.admin.email, 123, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.admin.create_date, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.admin.update_date, 146, 114);

        if (this.admin.photo) {
            this.toDataURL(this.admin.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.save('adminDetail.pdf');
            });
        } else {
            doc.save('adminDetail.pdf');
        }
    }

    toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob)
        }));

    printDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuario Administrador del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Nombre: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.admin.name, 34, 100);
        doc.setFontType("bold");
        doc.text('Apellido: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.admin.lastname, 123, 100);

        doc.setFontType("bold");
        doc.text('Cédula: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.admin.dni, 34, 107);
        doc.setFontType("bold");
        doc.text('Correo: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.admin.email, 123, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.admin.create_date, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.admin.update_date, 146, 114);

        if (this.admin.photo) {
            this.toDataURL(this.admin.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.autoPrint();
                window.open(doc.output('bloburl'), '_blank');
            });
        } else {
            doc.autoPrint();
            window.open(doc.output('bloburl'), '_blank');
        }
    }

    excelDetalle() {
        var excel = [];
        excel = [{'#' : this.admin.id, 'Cedula': this.admin.dni, 'Nombre':this.admin.name, 'Apellido':this.admin.lastname, 'Correo':this.admin.email, 'Fecha de creación':this.admin.create_date, 'Última actualización':this.admin.update_date}];
        this.excelService.exportAsExcelFile(excel, 'admindetail');
    }

     filterRole( role:string) {
        if ( role === 'COMPANY' || role === 'BASIC') {
           this.role = [{ value: 'BASIC', title: 'Usuario'}];
        }
    }

    setRole(role: string){
        console.log(this.selectedRole);
        if (this.selectedRole === 'Usuario'){
            this.panel = true;
        } else {
            this.panel = false;
        }

    }
    setPrincipal(param){
        console.log( this.permissions[param])
        this.permissions[param].enabled =  !this.permissions[param].enabled;
        if(this.permissions[param].enabled){
            this.permissions[param].see = true;
            this.permissions[param].edit = false;
        }else{
            this.permissions[param].see = false;
            this.permissions[param].edit = false;
        }
        console.log( this.permissions)

    }
    setPermission(param, number){
        if(this.permissions[param].enabled){
            if (number === 0){
                console.log('ver: ' + this.permissions[param].see);
                this.permissions[param].edit = !this.permissions[param].see;
                console.log('Modificar: ' + this.permissions[param].edit);
            } else{
                console.log('Modificar: ' + this.permissions[param].edit);
                this.permissions[param].see = !this.permissions[param].edit;
                console.log('ver: ' + this.permissions[param].see);
            }
        } else{
            alert('debe habilitar el modulo');
            this.permissions[param].see = false;
            this.permissions[param].edit = false;
        }
        console.log(this.permissions);
    }

}
