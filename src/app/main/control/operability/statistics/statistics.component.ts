import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as jsPDF from 'jspdf';
import {ToastrService} from 'ngx-toastr';
import {OperabilityService} from '../../../../../model/operability/operability.service';
import {ExcelService} from '../../../../../model/excel/excel.services';
import {PuestoService} from '../../../../../model/puestos/puestos.service';

@Component({
    selector: 'app-project',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent {
    // General
    data: any[] = [];
    devices: any[] = [];
    stands: any[] = [];
    isLoading = false;

    filter: string;
    numElement = 10;

    key = 'id'; // set default
    reverse = true;

    // dropdown
    dropdownList = [];
    selectedDevices = [];
    dropdownSettings = {};

    from = '';
    to = '';
    fromDate = '';
    toDate = '';

    // chart
    dataSource: any = {};

    chartWidth = '800px';
    chartHeight = '400px';
    showChart = false;
    enableChart = false;

    // Exports
    contentPDF: any = [];
    contentEXCEL: any = [];

    constructor(public router: Router,
                private operabilityService: OperabilityService,
                private standService: PuestoService,
                private excelService: ExcelService,
                private toastr: ToastrService) {
        this.getAllStands();
        this.setupDropdown();

        this.dataSource = {
            chart: {
                yAxisName: 'Porcentaje de operatividad (%)',
                yAxisMaxValue: 100
            },
            // Chart Data
            data: [
                {
                    label: 'Dispositivo',
                    color: '#dc3545',
                    value: 0
                }
            ]
        };

        const self = this;
        setTimeout(function () {
            const width = document.getElementById('container-chart').clientWidth;
            const height = document.documentElement.clientHeight;
            self.chartWidth = String(width);
            self.chartHeight = String(Number(height) * 0.6);
            self.enableChart = true;
        }, 3000);
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    setupDropdown() {
        this.dropdownList = [];
        this.selectedDevices = [];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Seleccionar todo',
            unSelectAllText: 'Deseleccionar todo',
            searchPlaceholderText: 'Buscar dispositivos...',
            itemsShowLimit: 100,
            allowSearchFilter: true,
            enableCheckAll: true,
        };
    }

    getAllStands() {
        this.devices = [];
        this.standService.getAll().then(
            (success: any) => {
                this.stands = success.data;
                this.getAllOperability();
            }, error => {
                this.isLoading = false;
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    getAllOperability() {
        this.operabilityService.getAll().then(
            (success: any) => {
                this.devices = success.data;
                this.devices.forEach(device => {
                    this.stands.forEach(stand => {
                        if (device.imei == stand.id) {
                            device.name = stand.name;
                        }
                    });
                });
                const data = [];
                this.devices.forEach(device => {
                    data.push({ item_id: device.imei, item_text: device.name });
                });
                this.dropdownList = data;
                this.isLoading = false;
            }, error => {
                this.isLoading = false;
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }
    showLoading() {
        this.isLoading = true;
        this.data = [];
    }

    dismissLoading() {
        this.isLoading = false;
    }

    search() {
        // format of dates
        const fromString = String(this.from);
        const valueFrom = fromString.split('-');
        const year = valueFrom[0];
        const month = valueFrom[1];
        const day = valueFrom[2];

        const toString = String(this.to);
        const valueTo = toString.split('-');
        const year_t = valueTo[0];
        const month_t = valueTo[1];
        const day_t = valueTo[2];

        this.fromDate = day + '/' + month + '/' + year;
        this.toDate = day_t + '/' + month_t + '/' + year_t;

        this.clearData();
        if (this.from == '' || this.to == '') {
            this.toastr.info('Selecciona el rango de fechas', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else if (this.selectedDevices.length == 0) {
            this.toastr.info('Selecciona los dispositivos a consultar', 'Error',
                { positionClass: 'toast-bottom-center'});
        } else {
            const array = [];
            this.selectedDevices.forEach(device => {
                array.push({imei: device.item_id});
            });
            this.showLoading();
            this.operabilityService.getHours(JSON.stringify(array), year, month, day, year_t, month_t, day_t)
                .then(this.onListSuccess.bind(this), this.onListFailure.bind(this));
        }
    }

    clearData() {
        this.data = [];
        this.dataSource.data = [
            {
                label: 'Puesto',
                    color: '#dc3545',
                value: 0
            }
        ];
    }

    onListSuccess(success) {
        if (this.isLoading) {
            success.data.sort((n1, n2) => {
                if (n1.hours_on > n2.hours_on) { return -1; }
                if (n1.hours_on < n2.hours_on) {return 1; }
                return 0;
            });
            this.data = success.data;
            const fromDate = new Date(this.from);
            const toDate = new Date(this.to);
            const diff = Math.abs(fromDate.getTime() - toDate.getTime());
            const diffDays = Math.ceil(diff / (1000 * 3600 * 24)) + 1;
            const totalHours = diffDays * 24;
            this.data.forEach(value => {
                const hoursOn = Math.round(value.minutes_on / 60);
                const hoursOff = Math.round(totalHours - hoursOn);
                const percentageOn = Math.round((hoursOn * 100) / totalHours);
                const percentageOff = Math.round((hoursOff * 100) / (diffDays * 24));
                value.hours_on = hoursOn;
                value.hours_off = hoursOff;
                value.percentage_on = percentageOn;
                value.percentage_off = percentageOff;
                this.stands.forEach(stand => {
                    if (value.imei == stand.id) {
                        value.name = stand.name;
                        value.address = stand.address;
                    }
                });
            });
            this.setChart();
            this.dismissLoading();
        }
    }

    onListFailure(error) {
        if (this.isLoading) {
            if (error.status === 422) {
                // on some data incorrect
            } else {
                // on general error
            }
            this.dismissLoading();
            this.toastr.info(error.message, 'Error',
                { positionClass: 'toast-bottom-center'});
        }
    }

    onItemSelect(item: any) {

    }

    onItemDeSelect(item: any) {
    }

    onSelectAll(items: any) {

    }

    onDeSelectAll(items: any) {

    }

    setChart() {
        this.dataSource.data = [];
        if (this.data.length > 0) {
            this.data.forEach(item => {
                this.dataSource.data.push({
                    label: item.name + ' Op',
                    color: '#28a745',
                    value: item.percentage_on
                });
                this.dataSource.data.push({
                    label: item.name + ' Nop',
                    color: '#dc3545',
                    value: item.percentage_off
                });
            });
        }
    }

    excelDownload() {
        this.setupPdfAndExcelData();
        this.excelService.exportAsExcelFile(this.contentEXCEL, 'operatividad');
    }

    pdfDownload() {
        this.setupPdfAndExcelData();
        const doc = this.getListPdf();
        doc.save('operatividad.pdf');
    }

    print() {
        this.setupPdfAndExcelData();
        const doc = this.getListPdf();
        doc.autoPrint();
        window.open(doc.output('bloburl'), '_blank');
    }

    getListPdf(): jsPDF {
        const doc = new jsPDF();
        doc.setFontSize(20);
        doc.text('Log Courier', 15, 20);
        doc.setFontSize(12);
        doc.setTextColor(100);
        const d = new Date();
        const date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear()
            + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        doc.text('Operatividad de Puestos', 15, 27);
        doc.text('Hora de impresión: ' + date, 15, 34);
        doc.text('Rango de fechas seleccionado: ' + this.fromDate + ' al ' + this.toDate, 15, 44);
        doc.autoTable({
            head: [['Puesto', 'Descripción', 'Horas OP', 'Horas Nop', '% OP', '% Nop']],
            body: this.contentPDF,
            startY: 48,
            columnStyles: {
                0: {cellWidth: 'auto'},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'},
                5: {cellWidth: 'auto'},
            }
        });
        return doc;
    }

    setupPdfAndExcelData() {
        const body = [];
        const excel = [];
        for (let i = 0; i < this.data.length; i++) {
            excel.push({
                'Puesto': this.data[i].name,
                'Descripcion': this.data[i].address,
                'Horas de Operativo': this.data[i].hours_on,
                'Horas de Inoperativo': this.data[i].hours_off,
                '% de Operativo': this.data[i].hours_on,
                '% de Inoperativo': this.data[i].hours_off
            });
            body.push([
                this.data[i].name,
                this.data[i].address,
                this.data[i].hours_on + ' horas',
                this.data[i].hours_off + ' horas',
                this.data[i].percentage_on + '%',
                this.data[i].percentage_off + '%'
            ]);
        }
        this.contentPDF = body;
        this.contentEXCEL = excel;
    }
}
