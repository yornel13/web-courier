import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GuardService } from '../../../../model/guard/guard.service';
import { Guard } from '../../../../model/guard/guard';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ExcelService } from '../../../../model/excel/excel.services';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../environments/environment';
import {Contact} from '../../../../model/contact/contact';
import {Vehicle} from '../../../../model/vehicle/vehicle';
import {VehiclesService} from '../../../../model/vehicle/vehicle.service';
import {CourierVehicleService} from '../../../../model/courier-vehicle/courier-vehicle.service';

@Component({
    selector: 'app-guardia',
    templateUrl: './guardia.component.html',
    styleUrls: ['./guardia.component.css']
})
export class GuardiaComponent {
    // General
    data: Guard[] = [];
    isLoading = false;

    driver: Guard = {};
    // vistas admin
    listView: boolean;
    detailsView: boolean;
    createView: boolean;
    editView: boolean;
    // edit
    nombre:string;
    apellido:string;
    correo:string;
    identificacion:string;
    photo:string;
    phone: string;
    contrasena:string = "password";
    idEdit:number;
    errorEdit:boolean = false;
    errorEditData:boolean = false;
    errorEditMsg:string;
    //createBoundView
    namea:string;
    lastnamea:string;
    emaila:string;
    dnia:string;
    photoa:string;
    phonea: string;
    passworda:string;
    errorSave:boolean = false;
    errorSaveData:boolean = false;
    errorNewMsg:string;
    //eliminar
    errorDelete:boolean = false;
    errorDeleteData:boolean = false;
    filter:string;
    //imagen firebase
    uploadPercent: Observable<number>;
    downloadURL: Observable<string>;
    numElement:number = 10;
    //exportaciones
    contpdf:any = [];
    info: any = [];
    key: string = 'id'; // set default
    reverse: boolean = true;

    filterValue = '';
    vehicles: any[] = [];
    guardToVehicle: Guard;

    constructor(
        public router: Router,
        private guardService: GuardService,
        private storage: AngularFireStorage,
        private excelService: ExcelService,
        private vehiclesService: VehiclesService,
        private courierVehicleService: CourierVehicleService,
        private toastr: ToastrService) {
        this.getAll();
        this.return();
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    getAll() {
        this.isLoading = true;
        this.data = [];
        this.guardService.getAll().then(
            (success: any) => {
                this.data = success.data;
                this.isLoading = false;
                var body = [];
                var excel = [];
                for (var i = 0; i < this.data.length; i++) {
                    // this.data[i].id = Number(this.data[i].id);
                    // this.data[i].dni = Number(this.data[i].dni);
                    // this.data[i].active = Number(this.data[i].active);
                    excel.push({'#' : this.data[i].id, 'Cedula': this.data[i].dni, 'Nombre': this.data[i].name,
                        'Apellido': this.data[i].lastname, 'Correo': this.data[i].email});
                    body.push([this.data[i].id, this.data[i].dni, this.data[i].name, this.data[i].lastname, this.data[i].email]);
                }
                this.contpdf = body;
                this.info = excel;
            }, error => {
                this.isLoading = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
        this.getVehicles();
    }

    getVehicles() {
        this.filterValue = '';
        this.vehicles = [];
        this.vehiclesService.getVehiclesList().then(
            (success: any) => {
                success.data.forEach(vehicle => {
                    this.vehicles.push(vehicle);
                });
                this.courierVehicleService.getAll().then(
                    (success: any) => {
                        success.data.forEach(vehicle => {
                            this.vehicles.push(vehicle);
                        });
                    }, error => {
                        this.toastr.info(environment.ERROR_GENERAL, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                );
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    selectVehicle(vehicle) {
        const plate = vehicle.plate ? vehicle.plate : vehicle.automotor_plate;
        this.guardToVehicle.vehicle_plate = plate;
        const newGuard: Guard = {
            id: this.guardToVehicle.id,
            dni: this.guardToVehicle.dni,
            name: this.guardToVehicle.name,
            lastname: this.guardToVehicle.lastname,
            email: this.guardToVehicle.email,
            photo: this.guardToVehicle.photo,
            phone: this.guardToVehicle.phone,
            vehicle_plate: plate
        };
        this.guardService.set(newGuard).then(
            (success: any) => {
                this.getAll();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    return() {
        this.listView = true;
        this.detailsView = false;
        this.createView = false;
        this.editView = false;
    }

    viewDetail(id) {
        this.guardService.getId(id).then(
            success => {
                this.driver = success;
                this.listView = false;
                this.detailsView = true;
                this.createView = false;
                this.editView = false;
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    // on general error
                }
            }
        );
    }

    upload(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        var url = '/icsse/' + randomId;
        const ref = this.storage.ref(url);
        // const task = ref.put(file);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(url => (this.photo = url));}
            )).subscribe();
    }

    uploadNew(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        var url = '/icsse/' + randomId;
        const ref = this.storage.ref(url);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(url => (this.photoa = url));}
            )).subscribe();
    }

    editarGuardia(id) {
        this.guardService.getId(id).then(
            success => {
                this.driver = success;
                this.nombre = this.driver.name;
                this.apellido = this.driver.lastname;
                this.correo = this.driver.email;
                this.phone = this.driver.phone;
                this.identificacion = this.driver.dni;
                this.idEdit = this.driver.id;
                if (this.driver.photo == null) {
                    this.photo = './assets/img/user_empty.jpg';
                } else {
                    this.photo = this.driver.photo;
                }
                this.listView = false;
                this.detailsView = false;
                this.createView = false;
                this.editView = true;
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    // on general error
                }
            }
        );
    }

    getValueEdit() {
        if (this.photo === './assets/img/user_empty.jpg') {
            this.photo = null;
        }
        if (this.contrasena === 'password') {
            return {
                id: this.idEdit,
                dni: this.identificacion,
                name: this.nombre,
                lastname: this.apellido,
                email: this.correo,
                photo: this.photo,
                phone: this.phone
            };
        } else {
            return {
                id: this.idEdit,
                dni: this.identificacion,
                name: this.nombre,
                lastname: this.apellido,
                email: this.correo,
                password: this.contrasena,
                photo: this.photo,
                phone: this.phone
            };
        }
    }

    saveEdit() {
        const values = this.getValueEdit();
        this.guardService.set(values).then(
            success => {
                this.getAll();
                this.return();
                this.photo = '';
                this.errorEditData = false;
                this.errorEdit = false;
            }, error => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'name') { title = 'Nombre'; }
                            if (title === 'lastname') { title = 'Apellido'; }
                            if (title === 'dni') { title = 'Cedula'; }
                            if (title === 'email') { title = 'Correo'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                    this.errorSaveData = true;
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                    this.errorSave = true;
                }
            }
        );
    }

    crearGuardia() {
        this.listView = false;
        this.detailsView = false;
        this.createView = true;
        this.editView = false;
        this.photoa = './assets/img/user_empty.jpg';
    }

    saveNewGuardia() {
        if(this.photoa == './assets/img/user_empty.jpg'){
            this.photoa = null;
        }
        const createguard: Guard = {
            dni: this.dnia,
            name: this.namea,
            lastname: this.lastnamea,
            email: this.emaila,
            password: this.passworda,
            photo: this.photoa,
            phone: this.phonea
        };
        this.guardService.add(createguard).then(
            success => {
                this.getAll();
                this.return();
                this.photoa = '';
                this.errorEditData = false;
                this.errorEdit = false;
            }, (error: any) => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'name') { title = 'Nombre'; }
                            if (title === 'lastname') { title = 'Apellido'; }
                            if (title === 'dni') { title = 'Cedula'; }
                            if (title === 'email') { title = 'Correo'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                    this.errorSaveData = true;
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                    this.errorSave = true;
                }
            }
        );
    }

    deleteGuardia(id) {
        this.guardService.delete(id).then(
            success => {
                this.getAll();
                this.return();
                this.errorDeleteData = false;
                this.errorDelete = false;
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                    this.errorDeleteData = true;
                } else {
                    // on general error
                    this.errorDelete = true;
                }
            }
        );
    }

    activarGuardia(id) {
        this.guardService.activeGuard(id).then(
            success => {
                this.getAll();
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    // on general error
                }
            }
        );
    }

    desactivarGuardia(id) {
        this.guardService.desactiveGuard(id).then(
            success => {
                this.getAll();
            }, error => {
                if (error.status === 422) {
                    // on some data incorrect
                } else {
                    // on general error
                }
            }
        );
    }

    pdfDownload() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuarios Guardias del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Cédula', 'Nombre', 'Apellido', 'Correo']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 10},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.save('guardias.pdf');
    }

    excelDownload() {
        this.excelService.exportAsExcelFile(this.info, 'guardias');
    }

    print() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuarios Guardias del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Cédula', 'Nombre', 'Apellido', 'Correo']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 10},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.autoPrint();
        window.open(doc.output('bloburl'), '_blank');
    }

    pdfDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuario Guardia del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Nombre: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.driver.name, 34, 100);
        doc.setFontType("bold");
        doc.text('Apellido: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.driver.lastname, 123, 100);

        doc.setFontType("bold");
        doc.text('Cédula: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.driver.dni, 34, 107);
        doc.setFontType("bold");
        doc.text('Correo: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.driver.email, 123, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.driver.create_date, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.driver.update_date, 146, 114);

        if (this.driver.photo) {
            this.toDataURL(this.driver.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.save('guardiaDetail.pdf');
            });
        }  else {
            doc.save('guardiaDetail.pdf');
        }
    }

    toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob)
        }));


    printDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuario Guardia del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Nombre: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.driver.name, 34, 100);
        doc.setFontType("bold");
        doc.text('Apellido: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.driver.lastname, 123, 100);

        doc.setFontType("bold");
        doc.text('Cédula: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.driver.dni, 34, 107);
        doc.setFontType("bold");
        doc.text('Correo: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.driver.email, 123, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.driver.create_date, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.driver.update_date, 146, 114);

        if (this.driver.photo) {
            this.toDataURL(this.driver.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.autoPrint();
                window.open(doc.output('bloburl'), '_blank');
            });
        } else {
            doc.autoPrint();
            window.open(doc.output('bloburl'), '_blank');
        }
    }

    excelDetalle() {
        var excel = [];
        excel = [{'#' : this.driver.id, 'Cedula': this.driver.dni, 'Nombre':this.driver.name, 'Apellido':this.driver.lastname, 'Correo':this.driver.email, 'Fecha de creación':this.driver.create_date, 'Última actualización':this.driver.update_date}];
        this.excelService.exportAsExcelFile(excel, 'admindetail');
    }
}
