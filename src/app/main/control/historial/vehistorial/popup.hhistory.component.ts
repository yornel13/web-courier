import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-popup-hhistory',
    templateUrl: './popup.hhistory.component.html',
    styleUrls: ['./popup.hhistory.css']
})
export class PopupHhistoryComponent implements OnInit {

    record: any;
    private date: Date;
    private time: Date;
    private isSOS: boolean;

    constructor() {}

    ngOnInit() {
      this.date = new Date(this.record.date);
      this.time = new Date(this.record.time);
      this.isSOS = this.record.alert_message.includes('SOS');
    }
}
