import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-popup-report',
    templateUrl: './popup.report.component.html',
    styleUrls: ['./popup.report.css']
})
export class PopupReportComponent implements OnInit {

    report: any;

    constructor() {}

    ngOnInit() { }
}
