import {Component, ComponentFactoryResolver, Injector, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import 'jspdf-autotable';
import { ExcelService } from '../../../../model/excel/excel.services';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import {GlobalOsm} from '../../../global.osm';
import {UtilsVehicles} from '../../../../model/vehicle/vehicle.utils';
import {BinnacleService} from '../../../../model/binnacle/binnacle.service';
import {Project} from '../../../../model/proyect/project';
import {AuthenticationService} from '../../../_services';
import {IncidenciasService} from '../../../../model/incidence/incidence.service';
import {environment} from '../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {Binnacle} from '../../../../model/binnacle/binnacle';
import {Admin} from '../../../../model/admin/admin';
import {Reply} from '../../../../model/binnacle/reply';
import {ApiResponse} from '../../../../model/app.response';
import * as uuid from 'uuid';

@Component({
    selector: 'app-binnacle',
    templateUrl: './binnacle.component.html',
    styleUrls: ['./binnacle.component.css']
})
export class BinnacleComponent implements OnInit {
    /* general */
    projectId;
    project: Project = {};
    data: any = [];
    isLoading = false;

    dataFilter;
    numElement = 10;
    p;

    listView = true;
    showView = false;

    incidences: any[] = [];
    incidenceSelected = 1;
    title: string;
    observation: string;

    key = 'id';
    reverse = true;

    lat = -2.0000;
    lng = -79.0000;
    map: L.Map;
    zoom;
    center = L.latLng(([ this.lat, this.lng ]));
    marker = L.marker([this.lat, this.lng], {draggable: false});
    markerClusterData: any[] = [];
    markerClusterOptions: L.MarkerClusterGroupOptions;
    layersControlOptions;
    baseLayers;
    options;
    private selectedReport: any;

    comment: string;

    comments: Reply[] = [];
    addComment = false;
    hasComments = false;

    constructor(
        private resolver: ComponentFactoryResolver,
        private globalOSM: GlobalOsm,
        private injector: Injector,
        private utilVehicle: UtilsVehicles,
        public router: Router,
        private binnacleService: BinnacleService,
        private excelService: ExcelService,
        private authService: AuthenticationService,
        private incidencesService: IncidenciasService,
        private toastr: ToastrService,
        private route: ActivatedRoute) {
        /* Map default options */
        this.layersControlOptions = this.globalOSM.layersOptions;
        this.baseLayers = this.globalOSM.baseLayers;
        this.options = this.globalOSM.defaultOptions;
        this.zoom = this.globalOSM.zoom;
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    ngOnInit() {
        this.project = this.authService.getProject();
        this.route.url.subscribe(value => {
            const projectId = value[value.length - 2].path;
            if (Number(projectId)) {
                this.projectId = projectId;
                this.getReports();
            }
        });
        this.getIncidences();
    }

    getIncidences() {
        this.incidencesService.getAll().then(
            (success: any) => {
                this.incidences = success.data;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getReports() {
        this.showLoading();
        this.data = [];
        this.binnacleService.getByProject(this.projectId)
            .then(this.onListReportsSuccess.bind(this), this.onListReportsFailure.bind(this));
    }

    onMapReady(map: L.Map) {
        this.map = map;
        this.globalOSM.setupLayer(this.map);
        this.zoom = this.globalOSM.fullZoom;
        this.center = L.latLng(([ this.lat, this.lng ]));
        this.marker = L.marker([this.lat, this.lng], { icon: L.icon({iconUrl: './assets/alerts/report.png'})} );
        this.marker.addTo(this.map);
    }

    showLoading() {
        this.isLoading = true;
        this.data = [];
    }

    dismissLoading() {
        this.isLoading = false;
    }

    viewDetail(selectReport) {
        this.selectedReport = selectReport;
        this.binnacleService.getId(this.selectedReport.id).then(
            success => {
                this.repairReport(success);
                this.selectedReport = success;
                if (this.selectedReport.latitude != null) {
                    this.selectedReport.latitude = this.lat = Number(this.selectedReport.latitude);
                    this.selectedReport.longitude = this.lng = Number(this.selectedReport.longitude);
                }
                this.listView = false;
                this.showView = true;
                this.zoom = this.globalOSM.fullZoom;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );

        this.getComments(this.selectedReport.id);
    }

    getComments(id) {
        this.binnacleService.getComments(id).then((success: any) => {
                this.comments = success.data;
                if (this.comments.length === 0) {
                    this.hasComments = false;
                } else {
                    this.hasComments = true;
                }
                // this.putReportRead(id);
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    returnList() {
        this.listView = true;
        this.showView = false;
    }

    onListReportsSuccess(success: any) {
        if (this.isLoading) {
            success.data.forEach(report => {
                this.repairReport(report);
            });
            this.data = success.data;
            this.dismissLoading();
        }
    }

    onListReportsFailure(error) {
        if (this.isLoading) {
            if (error.status === 422) {
                // on some data incorrect
            } else {
                // on general error
            }
            this.dismissLoading();
        }
    }

    create() {
        const randomId = uuid.v4();
        const report: Binnacle = {
            title: this.title,
            observation: this.observation,
            incidence_id: this.incidenceSelected,
            admin_id: this.authService.getUser().id,
            project_id: this.projectId,
            sync_id: randomId
        };
        this.binnacleService.save(report).then(
            (success: any) => {
                this.title = '';
                this.observation = '';
                this.incidenceSelected = 1;
                // this.data.push(contact);
                this.getReports();
                this.toastr.success('El reporte fue creado con exito!.', '',
                    { positionClass: 'toast-bottom-center'});
            }, error => {
                if (error.status === 422 && error.error && error.error.message) {
                    this.toastr.info(error.error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                } else {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    saveComment() {
        const admin: Admin = this.authService.getUser();
        const comment: Reply = {
            binnacle_id: this.selectedReport.id,
            text: this.comment,
            admin_id:  admin.id,
            user_name: admin.name + ' ' + admin.lastname,
        };
        this.binnacleService.addComment(comment).then(
            success => {
                this.viewDetail(this.selectedReport);
                this.addComment = false;
                this.comment = '';
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    changeResolve(report, resolved) {
        if (resolved === 0) {
            this.binnacleService.setReopen(report.id).then(
                (success: ApiResponse) => {
                    report.resolved = success.result.resolved;
                    this.selectedReport.resolved = success.result.resolved;
                }, error => {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            );
        } else {
            this.binnacleService.setClose(report.id).then(
                (success: ApiResponse) => {
                    report.resolved = success.result.resolved;
                    this.selectedReport.resolved = success.result.resolved;
                }, error => {
                    this.toastr.info(environment.ERROR_GENERAL, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            );
        }
    }

    repairReport(report: any): any {
        report.user_title = report.title;
        if (report.title === 'STARTED_TRACKING') {
            report.user_title = 'Tracking comenzado';
            report.observation = 'El conductor a comenzado el tracking del proyecto.';
        } else if (report.title === 'PAUSE_TRACKING') {
            report.user_title = 'Tracking comenzado';
            report.observation = 'El conductor se ha desconectado del tracking del proyecto.';
        } else if (report.title === 'RESUMED_TRACKING') {
            report.user_title = 'Tracking retomado';
            report.observation = 'El conductor se ha retomado el tracking del proyecto.';
        } else if (report.title === 'FINISHED_TRACKING') {
            report.user_title = 'Tracking finalizado';
            report.observation = 'El conductor ha finalizado el tracking.';
        } else if (report.title === 'PICKUP') {
            report.user_title = 'Carga Recibida';
        } else if (report.title === 'DELIVERY') {
            report.user_title = 'Carga Entregada';
        }
        return report;
    }
}
