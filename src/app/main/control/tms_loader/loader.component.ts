import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GuardService } from '../../../../model/guard/guard.service';
import { Guard } from '../../../../model/guard/guard';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ExcelService } from '../../../../model/excel/excel.services';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../environments/environment';
import {Loader} from '../../../../model/loader/loader';
import {LoaderService} from '../../../../model/loader/loader.service';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.css']
})
export class LoaderComponent {
    // General
    data: Loader[] = [];
    isLoading = false;

    loader: Loader = {};
    // vistas admin
    listView: boolean;
    showView: boolean;
    createView: boolean;
    editView: boolean;
    // edit
    name: string;
    lastname: string;
    dni: string;
    phone: string;
    photo: string;
    idEdit: number;

    // imagen firebase
    uploadPercent: Observable<number>;
    downloadURL: Observable<string>;
    numElement:number = 10;
    //exportaciones
    contpdf:any = [];
    info: any = [];
    key: string = 'id'; // set default
    reverse: boolean = true;


    constructor(
        public router: Router,
        private loaderService: LoaderService,
        private storage: AngularFireStorage,
        private excelService: ExcelService,
        private toastr: ToastrService) {
        this.getAll();
        this.return();
    }

    sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    getAll() {
        this.isLoading = true;
        this.data = [];
        this.loaderService.getAll().then(
            (success: any) => {
                this.data = success.data;
                this.isLoading = false;
                const body = [];
                const excel = [];
                for (let i = 0; i < this.data.length; i++) {
                    excel.push({'#' : this.data[i].id, 'Cedula': this.data[i].dni, 'Nombre': this.data[i].name,
                        'Apellido': this.data[i].lastname, 'Telefono': this.data[i].phone});
                    body.push([this.data[i].id, this.data[i].dni, this.data[i].name, this.data[i].lastname, this.data[i].phone]);
                }
                this.contpdf = body;
                this.info = excel;
            }, error => {
                this.isLoading = false;
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    return() {
        this.listView = true;
        this.showView = false;
        this.createView = false;
        this.editView = false;
    }

    viewDetail(id) {
        this.name = '';
        this.lastname = '';
        this.dni = '';
        this.phone = '';
        this.loaderService.getId(id).then(
            success => {
                this.loader = success;
                this.listView = false;
                this.showView = true;
                this.createView = false;
                this.editView = false;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    upload(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        const url = '/courier/' + randomId;
        const ref = this.storage.ref(url);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(_url => (this.photo = _url)); }
            )).subscribe();
    }

    uploadNew(event) {
        const file = event.target.files[0];
        const randomId = Math.random().toString(36).substring(2);
        const url = '/courier/' + randomId;
        const ref = this.storage.ref(url);
        const task = this.storage.upload(url, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(
            finalize(() => {this.downloadURL = ref.getDownloadURL();
                this.downloadURL.subscribe(_url => (this.photo = _url)); }
            )).subscribe();
    }

    editLoader(id) {
        this.loaderService.getId(id).then(
            success => {
                this.loader = success;
                this.name = this.loader.name;
                this.lastname = this.loader.lastname;
                this.phone = this.loader.phone;
                this.dni = this.loader.dni;
                this.idEdit = this.loader.id;
                if (this.loader.photo == null) {
                    this.photo = './assets/img/user_empty.jpg';
                } else {
                    this.photo = this.loader.photo;
                }
                this.listView = false;
                this.showView = false;
                this.createView = false;
                this.editView = true;
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    getValueEdit() {
        if (this.photo === './assets/img/user_empty.jpg') {
            this.photo = null;
        }
        return {
            id: this.idEdit,
            dni: this.dni,
            name: this.name,
            lastname: this.lastname,
            phone: this.phone,
            photo: this.photo
        };
    }

    saveEdit() {
        const loaderValues = this.getValueEdit();
        this.loaderService.set(loaderValues).then(
            success => {
                this.getAll();
                this.return();
                this.photo = '';
            }, error => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'name') { title = 'Nombre'; }
                            if (title === 'lastname') { title = 'Apellido'; }
                            if (title === 'dni') { title = 'Cedula'; }
                            if (title === 'phone') { title = 'Telefono'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    showCreateView() {
        this.listView = false;
        this.showView = false;
        this.createView = true;
        this.editView = false;

        this.dni = undefined;
        this.name = undefined;
        this.lastname = undefined;
        this.phone = undefined;
        this.photo = './assets/img/user_empty.jpg';
    }

    saveLoader() {
        if (this.photo === './assets/img/user_empty.jpg') {
            this.photo = null;
        }
        const newLoader: Loader = {
            dni: this.dni,
            name: this.name,
            lastname: this.lastname,
            phone: this.phone,
            photo: this.photo
        };
        this.loaderService.add(newLoader).then(
            success => {
                this.getAll();
                this.return();
                this.photo = '';
            }, (error: any) => {
                if (error.status === 422) {
                    if (error.error.errors) {
                        for (const key in error.error.errors) {
                            let title = key;
                            if (title === 'name') { title = 'Nombre'; }
                            if (title === 'lastname') { title = 'Apellido'; }
                            if (title === 'dni') { title = 'Cedula'; }
                            if (title === 'phone') { title = 'Telefono'; }
                            this.toastr.info(error.error.errors[key][0], title,
                                { positionClass: 'toast-bottom-center'});
                        }
                    } else {
                        this.toastr.info(error.error.message, 'Error',
                            { positionClass: 'toast-bottom-center'});
                    }
                } else {
                    this.toastr.info(error.message, 'Error',
                        { positionClass: 'toast-bottom-center'});
                }
            }
        );
    }

    deleteLoader(id) {
        this.loaderService.delete(id).then(
            success => {
                this.getAll();
                this.return();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    activeLoader(id) {
        this.loaderService.activeLoader(id).then(
            success => {
                this.getAll();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    disableLoader(id) {
        this.loaderService.disableLoader(id).then(
            success => {
                this.getAll();
            }, error => {
                this.toastr.info(environment.ERROR_GENERAL, 'Error',
                    { positionClass: 'toast-bottom-center'});
            }
        );
    }

    pdfDownload() {
        var doc = new jsPDF();
        doc.setFontSize(20);
        doc.text('Log Courier', 15, 20);
        doc.setFontSize(12);
        doc.setTextColor(100);
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuarios Guardias del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Cédula', 'Nombre', 'Apellido', 'Telefono']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 10},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.save('guardias.pdf');
    }

    excelDownload() {
        this.excelService.exportAsExcelFile(this.info, 'guardias');
    }

    print() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Estibas Registrados', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34)
        doc.autoTable({
            head: [['#', 'Cédula', 'Nombre', 'Apellido', 'Telefono']],
            body: this.contpdf,
            startY: 41,
            columnStyles: {
                0: {cellWidth: 10},
                1: {cellWidth: 'auto'},
                2: {cellWidth: 'auto'},
                3: {cellWidth: 'auto'},
                4: {cellWidth: 'auto'}
            }
        });
        doc.autoPrint();
        window.open(doc.output('bloburl'), '_blank');
    }

    pdfDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuario Guardia del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Nombre: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.loader.name, 34, 100);
        doc.setFontType("bold");
        doc.text('Apellido: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.loader.lastname, 123, 100);

        doc.setFontType("bold");
        doc.text('Cédula: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.loader.dni, 34, 107);
        doc.setFontType("bold");
        doc.text('Telefono: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.loader.phone, 123, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.loader.create_at, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.loader.update_at, 146, 114);

        if (this.loader.photo) {
            this.toDataURL(this.loader.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.save('guardiaDetail.pdf');
            });
        }  else {
            doc.save('guardiaDetail.pdf');
        }
    }

    toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob);
        }));


    printDetalle() {
        var doc = new jsPDF();
        doc.setFontSize(20)
        doc.text('Log Courier', 15, 20)
        doc.setFontSize(12)
        doc.setTextColor(100)
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        doc.text('Usuario Guardia del sistema', 15, 27)
        doc.text('Hora de impresión: '+ fecha, 15, 34);
        //inserting data
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Nombre: ', 15, 100);
        doc.setFontType("normal");
        doc.text(this.loader.name, 34, 100);
        doc.setFontType("bold");
        doc.text('Apellido: ', 100, 100);
        doc.setFontType("normal");
        doc.text(this.loader.lastname, 123, 100);

        doc.setFontType("bold");
        doc.text('Cédula: ', 15, 107);
        doc.setFontType("normal");
        doc.text(this.loader.dni, 34, 107);
        doc.setFontType("bold");
        doc.text('Telefono: ', 100, 107);
        doc.setFontType("normal");
        doc.text(this.loader.phone, 123, 107);

        doc.setFontType("bold");
        doc.text('Fecha de creación: ', 15, 114);
        doc.setFontType("normal");
        doc.text(this.loader.create_at, 56, 114);
        doc.setFontType("bold");
        doc.text('Última actualización: ', 100, 114);
        doc.setFontType("normal");
        doc.text(this.loader.update_at, 146, 114);

        if (this.loader.photo) {
            this.toDataURL(this.loader.photo).then(dataUrl => {
                var imgData = dataUrl;
                doc.addImage(imgData, 'JPEG', 15, 45, 40, 40);
                doc.autoPrint();
                window.open(doc.output('bloburl'), '_blank');
            });
        } else {
            doc.autoPrint();
            window.open(doc.output('bloburl'), '_blank');
        }
    }

    excelDetalle() {
        var excel = [];
        excel = [{'#' : this.loader.id, 'Cedula': this.loader.dni, 'Nombre':this.loader.name, 'Apellido':this.loader.lastname, 'Telefono':this.loader.phone, 'Fecha de creación':this.loader.create_at, 'Última actualización':this.loader.update_at}];
        this.excelService.exportAsExcelFile(excel, 'admindetail');
    }
}
