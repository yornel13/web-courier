import {Component, OnInit} from '@angular/core';
import {MessagingService} from '../../../shared/messaging.service';


@Component({
    selector: 'app-control-aside',
    templateUrl: './aside.control.component.html',
    styleUrls: ['./aside.control.component.css']
})

export class AsideControlComponent implements OnInit {

    public role: string;
    unreadReplies: number;
    permit = true;

    constructor(private messagingService: MessagingService) {
    }

    ngOnInit() {
        // this.role = localStorage.getItem('ROLE');
        // this.fliterView(this.role);
        this.unreadReplies = this.messagingService.repliesUnread;
        this.messagingService.repliesUnreadEmitter.subscribe(count => {
            this.unreadReplies = count;
        });
    }

    // fliterView(role: string) {
    //     if (role === 'MASTER' || role === 'ADMIN') {
    //         this.permit = true;
    //     } else {
    //         this.permit = false;
    //     }
    // }
}
