import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { CommonModule } from '@angular/common';

import {PopupVehicleComponent} from './monitoring/map/popup.vehicle.component';
import {PopupWatchComponent} from './monitoring/map/popup.watch.component';

import { VehiclesService } from '../../model/vehicle/vehicle.service';
import { WatchesService } from '../../model/watch/watch.service';
import {CardVehicleComponent} from './monitoring/aside/card.vehicle.component';
import {CardTabletComponent} from './monitoring/aside/card.tablet.component';

import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import { FusionChartsModule } from 'angular-fusioncharts';
import FusionCharts from 'fusioncharts/core';
import Column2d from 'fusioncharts/viz/column2d';
FusionChartsModule.fcRoot(FusionCharts, Column2d);

// My Modulus
import { MonitoringComponent } from './monitoring/monitoring.component';
import { MapOsmComponent } from './monitoring/map/map.osm';
import { HeaderComponent } from './header/header.component';
import { AsideComponent } from './monitoring/aside/aside.component';
import { MainComponent } from './main.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReportComponent } from './report/report.component';
import {RouterModule, Routes} from '@angular/router';
import {MainService} from './main.service';
import { MessagingComponent } from './messaging/messaging.component';

import {FilterMarkerPipe} from './monitoring/aside/filter.pipe';
import { FilterPipeModule } from 'ngx-filter-pipe';
import {TabletService} from '../../model/tablet/tablet.service';
import {ControlModule} from './control/control.module';
import {CardAlertComponent} from './monitoring/aside/card.alert.component';
import {ItemGuardComponent} from './messaging/item.guard.component';
import {PopupAlertComponent} from './monitoring/map/popup.alert.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {FilterUsers} from './messaging/filter.users';
import {RelativeTimePipe} from './messaging/relative.pipe';
import {CardRecordComponent} from './monitoring/aside/card.record.component';
import {ToastrModule} from 'ngx-toastr';
import {HistoryPrint} from './monitoring/history.print';
import {InfolinePrint} from './monitoring/infoline.print';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ErrorInterceptor, JwtInterceptor} from '../_helpers';
import {ErrorService} from '../../model/error/error.service';
import {AngularSplitModule} from 'angular-split';
import {TracingComponent} from './monitoring/tracing/tracing.component';
import {ProjectService} from '../../model/proyect/project.service';
import {PointService} from '../../model/point/point.service';
import {ContactService} from '../../model/contact/contact.service';
import {RegionsService} from '../../model/regions/regions.service';
import {OrderService} from '../../model/order/order.service';
import {RouteService} from '../../model/route/route.service';
import {OrderContactService} from '../../model/contact/order.contact.service';
import {BinnacleService} from '../../model/binnacle/binnacle.service';
import {CourierVehicleService} from '../../model/courier-vehicle/courier-vehicle.service';
import {LoaderService} from '../../model/loader/loader.service';
import {PositionService} from '../../model/position/position.service';
import {PopupPositionComponent} from './monitoring/map/popup.position.component';
import {FilterPipe} from './monitoring/tracing/filter.pipe';

export function controlModuleLoader() {
    return ControlModule;
}

export const mainRoutes: Routes = [
    { path: '', component: MainComponent,
        children: [
            {
                path: '',
                children: [
                    { path: 'dashboard', component: DashboardComponent },
                    { path: 'monitoring', component: MonitoringComponent },
                    { path: 'control', loadChildren: './control/control.module#ControlModule' },
                    { path: 'messaging', component: MessagingComponent },
                    { path: 'report', component: ReportComponent },
                    { path: '', redirectTo: '/u/dashboard', pathMatch: 'full' },
                ]
            }
        ]
    }
];

export const mainRouting = RouterModule.forChild(mainRoutes);

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        NgSelectModule,
        FormsModule,
        ReactiveFormsModule,
        mainRouting,
        ChartsModule,
        FusionChartsModule,
        FilterPipeModule,
        LeafletModule.forRoot(),
        LeafletMarkerClusterModule.forRoot(),
        LeafletDrawModule.forRoot(),
        ControlModule,
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        }),
        NgMultiSelectDropDownModule.forRoot(),
        ToastrModule.forRoot(),
        AngularSplitModule.forRoot()
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        ProjectService, PointService, RegionsService, ContactService, OrderService, RouteService, OrderContactService,
        BinnacleService, CourierVehicleService, LoaderService, PositionService,
        VehiclesService, WatchesService, MainService, TabletService, HistoryPrint, InfolinePrint, ErrorService ],
    entryComponents: [ PopupVehicleComponent, PopupWatchComponent, PopupAlertComponent, CardVehicleComponent, PopupPositionComponent,
        CardTabletComponent, ItemGuardComponent, CardTabletComponent ],
    declarations: [
        MonitoringComponent, MapOsmComponent, PopupVehicleComponent, PopupWatchComponent, PopupAlertComponent, PopupPositionComponent,
        HeaderComponent, AsideComponent, TracingComponent, MainComponent,
        DashboardComponent, ReportComponent, MessagingComponent,
        CardVehicleComponent, CardTabletComponent, FilterMarkerPipe, CardAlertComponent, ItemGuardComponent, FilterUsers,
        RelativeTimePipe, CardRecordComponent, FilterPipe
    ],
    bootstrap: [ MainComponent ]
})
export class MainModule {}
