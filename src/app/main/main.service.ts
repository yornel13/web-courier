import {EventEmitter, Injectable} from '@angular/core';
import {Alerta} from '../../model/alerta/alerta';
import {LatLng} from 'leaflet';

@Injectable({
    providedIn: 'root',
})
export class MainService {

    alerts: Alerta[] = [];
    alerts0: Alerta[] = [];
    alerts1: Alerta[] = [];

    records: any[] = [];

    selectedDevice: any;

    marker = new EventEmitter<any>();
    vehicle = new EventEmitter<any>();
    alertsEmitter = new EventEmitter<Alerta[]>();
    alerts0Emitter = new EventEmitter<Alerta[]>();
    alerts1Emitter = new EventEmitter<Alerta[]>();

    recordsEmitter = new EventEmitter<any[]>();
    recordsOrderEmitter = new EventEmitter<any[]>();
    routesOrderEmitter = new EventEmitter<any[]>();

    createPointEmitter = new EventEmitter<boolean>();
    pointerSelectedEmitter = new EventEmitter<LatLng>();

    showVehicles = new EventEmitter<any>();
}
