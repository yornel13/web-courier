export interface Reply {
  id?: number;
  binnacle_id?: string;
  text?: string;
  admin_id?: number;
  guard_id?: number;
  user_name?: string;
}
