import {Project} from '../proyect/project';
import {Guard} from '../guard/guard';
import {Admin} from '../admin/admin';
import {Incidence} from '../incidence/incidence';

export interface Binnacle {
    id?: number;
    project_id?: number;
    admin_id?: number;
    incidence_id?: number;
    guard_id?: number;
    title?: string;
    observation?: string;
    latitude?: string;
    longitude?: string;
    create_at?: string;
    update_at?: string;
    status?: number;
    sync_id?: string;

    position_id?: number;
    resolved?: number;
    level?: number;
    project?: Project;
    incidence?: Incidence;
    guard?: Guard;
    admin?: Admin;
}
