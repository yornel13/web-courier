import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {Region} from './region';
import {City} from './city';
import {AuthenticationService} from '../../app/_services';

@Injectable()
export class RegionsService {

    private REGIONS_URL = environment.BASIC_URL + '/region';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    getAllRegions() {
        return this.http.get<Region>(this.REGIONS_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAllCities() {
        return this.http.get<City>(this.REGIONS_URL + '/city',
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

}
