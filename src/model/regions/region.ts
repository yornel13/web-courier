export interface Region {
    id?: number;
    name?: string;
    county_id?: number;
}
