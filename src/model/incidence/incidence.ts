export interface Incidence {
    id?: number;
    level?: string;
    name?: string;
}