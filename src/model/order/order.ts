export interface Order {
    id?: number;
    project_id?: number;
    cost_center?: string;
    loaders?: number;
    routers?: number;

    type_dispatch: boolean;
    type_withdrawal: boolean;
    type_movements: boolean;

    access_terrace: boolean;
    access_hill: boolean;
    access_river: boolean;
    access_restriction: boolean;
    access_load500: boolean;
    access_narrow: boolean;

    truck_350kg: boolean;
    truck_550kg: boolean;
    truck_900kg: boolean;
    truck_1tn: boolean;
    truck_2tn: boolean;
    truck_5tn: boolean;
    truck_7tn: boolean;
    truck_12tn: boolean;
    truck_18tn: boolean;
    truck_low: boolean;

    additional_container20: boolean;
    additional_container40: boolean;
    additional_trailer40: boolean;
    additional_trailer48: boolean;
    additional_trailer53: boolean;
    additional_platform8: boolean;
    additional_platform12: boolean;
    additional_forklift_1tn: boolean;
    additional_forklift_2tn: boolean;
    additional_forklift_5tn: boolean;
    additional_truck_4x4: boolean;
    additional_barge: boolean;
    additional_crane_25tn: boolean;
    additional_packer: boolean;

    observation?: string;
    create_at?: string;
    update_at?: string;
    status?: string;
}
