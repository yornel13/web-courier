import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {Order} from './order';
import {AuthenticationService} from '../../app/_services';
@Injectable()
export class OrderService {

    private ORDER_URL = environment.BASIC_URL + '/project/order';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(order: Order) {
        return this.http.post(this.ORDER_URL, order,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    set(order: Order) {
        return this.http.put(this.ORDER_URL + '/' + order.id, order,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<Order>(this.ORDER_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.ORDER_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getByProject(id: number) {
        return this.http.get(this.ORDER_URL + '/project/' + id,
            {
                headers: this.authService.getHeader()
            })
        .toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.ORDER_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

}
