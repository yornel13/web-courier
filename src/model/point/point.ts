export interface Point {
    id?: number;
    name?: string;
    province?: string;
    city?: string;
    address?: string;
    latitude?: string;
    longitude?: string;
    comment?: string;
    type?: string;
    create_at?: string;
    update_at?: string;
    status?: string;
    registered_by?: number;
    company_id?: number;
}
