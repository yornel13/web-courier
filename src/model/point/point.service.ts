import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {Point} from './point';
import {AuthenticationService} from '../../app/_services';

@Injectable()
export class PointService {

    private POINT_URL = environment.BASIC_URL + '/point';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(route: Point) {
        return this.http.post(this.POINT_URL, route,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    set(route: Point) {
        return this.http.put(this.POINT_URL + '/' + route.id, route,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<Point>(this.POINT_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.POINT_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.POINT_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
