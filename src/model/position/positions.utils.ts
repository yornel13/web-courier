import {Position} from './position';
import {Guard} from '../guard/guard';
import {Project} from '../proyect/project';

export class PositionsUtils {

    static UPDATE = 'UPDATE';
    static STARTED_TRACKING = 'STARTED_TRACKING';
    static PAUSE_TRACKING = 'PAUSE_TRACKING';
    static RESUMED_TRACKING = 'RESUMED_TRACKING';
    static FINISHED_TRACKING = 'FINISHED_TRACKING';
    static PICKUP = 'PICKUP';
    static DELIVERY = 'DELIVERY';
    static INCIDENCE_LEVEL_1 = 'INCIDENCE_LEVEL_1';
    static INCIDENCE_LEVEL_2 = 'INCIDENCE_LEVEL_2';
    static SOS = 'SOS';

    processPositions(positions: any[]): any[] {
        positions.forEach(position => {
            position.index = positions.indexOf(position) + 1;
            position.full_name = position.driver.name + ' ' + position.driver.lastname;
            position.latitude = parseFloat(position.latitude).toFixed(4);
            position.longitude = parseFloat(position.longitude).toFixed(4);
            this.processMessage(position);
        });
        return positions;
    }

    processPositionsFake(positions: any[]): any[] {
        const customPositions: Position[] = [];
        positions.forEach((position: Position) => {
            if (/*position.is_exception || */position.custom) {
                customPositions.push(position)
            }
        });
        return this.processPositions(customPositions);
    }

    private processMessage(position: any): any {
        position.user_message = position.message;
        if (position.message.includes(PositionsUtils.UPDATE)) {
            position.user_message = 'Actualización de posición';
        } else if (position.message.includes(PositionsUtils.STARTED_TRACKING)) {
            position.user_message = 'Comienzo de tracking';
        } else if (position.message.includes(PositionsUtils.PAUSE_TRACKING)) {
            position.user_message = 'Tracking pausado';
        } else if (position.message.includes(PositionsUtils.RESUMED_TRACKING)) {
            position.user_message = 'Tracking retomado';
        } else if (position.message.includes(PositionsUtils.FINISHED_TRACKING)) {
            position.user_message = 'Tracking finalizado';
        } else if (position.message.includes(PositionsUtils.PICKUP)) {
            position.user_message = 'Carga PICKUP';
        } else if (position.message.includes(PositionsUtils.DELIVERY)) {
            position.user_message = 'Carga Entregada';
        } else if (position.message.includes(PositionsUtils.INCIDENCE_LEVEL_1)) {
            position.user_message = 'Incidencia Registrada';
        } else if (position.message.includes(PositionsUtils.INCIDENCE_LEVEL_2)) {
            position.user_message = 'Incidencia Importante';
        } else if (position.message.includes(PositionsUtils.SOS)) {
            position.user_message = 'SOS';
        }
    }

    processPositionsAndHistory(phonePositions: Position[], vehicleHistory: any[]): Position[] {
        let vehiclePositions: Position[] = [];
        const driver: Guard = phonePositions[0].driver;
        const project: Project = phonePositions[0].project;
        vehicleHistory.forEach(vHistory => {
            const date = new Date(vHistory.date + ' ' + vHistory.time);
            if (!vHistory.is_exception) {
                vehiclePositions.push({
                    is_exception: false,
                    latitude: vHistory.latitude,
                    longitude: vHistory.longitude,
                    generated_time: String(date),
                    message_time: String(date),
                    driver: driver,
                    project: project,
                    alert_message: vHistory.alert_message,
                    imei: driver.claro_vehicle.imei,
                    project_id: project.id,
                    driver_id: driver.id,
                    message: PositionsUtils.UPDATE
                });
            }
        });
        const alertPositions: Position[] = [];
        phonePositions.forEach((position: Position) => {
            if (position.is_exception) {
                alertPositions.push(position);
            }
        });
        /* Concat both arrays */
        vehiclePositions = vehiclePositions.concat(alertPositions);

        /* Sort array per generated_time */
        vehiclePositions.sort((a: Position, b: Position) => {
            return new Date(a.generated_time).getTime() - new Date(b.generated_time).getTime();
        });

        /* Process positions info */
        vehiclePositions.forEach((position: Position) => {
            position.index = vehiclePositions.indexOf(position) + 1;
            position.full_name = position.driver.name + ' ' + position.driver.lastname;
            position.latitude = parseFloat(position.latitude).toFixed(4);
            position.longitude = parseFloat(position.longitude).toFixed(4);
            this.processMessage(position);
        });

        return vehiclePositions;
    }
}














