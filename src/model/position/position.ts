import {Guard} from '../guard/guard';
import {Project} from '../proyect/project';

export interface Position {
    id?: number;
    latitude?: string;
    longitude?: string;
    driver_id?: number;
    project_id?: number;
    route_id?: number;
    generated_time?: string;
    message_time?: string;
    imei?: string;
    message?: string;
    alert_message?: string;
    is_exception?: boolean;
    binnacle_sync_id?: string;
    driver?: Guard;
    project?: Project
    index?: number,
    full_name?: string,
    custom?: boolean
}
