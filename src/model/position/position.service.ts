import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {AuthenticationService} from '../../app/_services';
import {Position} from './position';
import {map} from 'rxjs/operators';

@Injectable()
export class PositionService {

    private POSITION_URL = environment.BASIC_URL + '/project/position';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    getByProject(projectId: number) {
        return this.http.get(this.POSITION_URL + '/project/' + projectId,
            {
                headers: this.authService.getHeader()
            }).pipe(map((response: any) => {
                response.data.sort((a: Position, b: Position) => {
                    return new Date(a.generated_time).getTime() - new Date(b.generated_time).getTime();
                });
                return response;
            }
        )).toPromise();
    }

    getByProjectWithExeption(projectId: number) {
        return this.http.get(this.POSITION_URL + '/project/' + projectId,
            {
                headers: this.authService.getHeader()
            }).pipe(map((response: any) => {
                const responseE: any[] = [];
                response.data.forEach((position: Position) => {
                   if (position.is_exception) {
                       responseE.push(position);
                   }
                });
                response.data = responseE;
                response.data.sort((a: Position, b: Position) => {
                    return new Date(a.generated_time).getTime() - new Date(b.generated_time).getTime();
                });
                return response;
            }
        )).toPromise();
    }

    getByProjectAll(projectId: number) {
        return this.http.get(this.POSITION_URL + '/project/' + projectId + '/all',
            {
                headers: this.authService.getHeader()
            }).pipe(map((response: any) => {
                response.data.sort((a: Position, b: Position) => {
                    return new Date(a.generated_time).getTime() - new Date(b.generated_time).getTime();
                });
                return response;
            }
        )).toPromise();
    }

    getByRoute(routeId: number) {
        return this.http.get(this.POSITION_URL + '/route/' + routeId,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getByDriver(driverId: number) {
        return this.http.get(this.POSITION_URL + '/driver/' + driverId,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    sync(position: Position) {
        return this.http.post(this.POSITION_URL + '/sync', position,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.POSITION_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            })
            .toPromise().then((response) => response);
    }
}
