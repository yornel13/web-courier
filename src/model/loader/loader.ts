export interface Loader {
  id?: number;
  dni?: string;
  name?: string;
  lastname?: string;
  phone?: string;
  photo?: string;
  create_at?: string;
  update_at?: string;
  active?: boolean;
}
