import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {Loader} from './loader';
import {AuthenticationService} from '../../app/_services';

@Injectable()
export class LoaderService {

    private LOADER_URL = environment.BASIC_URL + '/loader';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(loader: Loader) {
        return this.http.post(this.LOADER_URL, loader,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    set(loader: Loader) {
        return this.http.put(this.LOADER_URL + '/' + loader.id, loader,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    activeLoader(id: number) {
        return this.http.put(this.LOADER_URL + '/' + id + '/active/1',
            {},
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    disableLoader(id: number) {
        return this.http.put(this.LOADER_URL + '/' + id + '/active/0',
            {},
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<Loader>(this.LOADER_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAllActive() {
        return this.http.get<Loader>(this.LOADER_URL + '/active/1',
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.LOADER_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.LOADER_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
