import {Project} from '../proyect/project';
import {Loader} from './loader';

export interface AssignedLoader {
    id?: number;
    loader_id?: number;
    project_id?: number;
    order_id?: number;
    create_at?: string;
    update_at?: string;
    status?: string;
    active?: boolean;
    loader?: Loader;
    project?: Project;
    order?: Project;
}
