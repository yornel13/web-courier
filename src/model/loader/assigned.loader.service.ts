import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {AuthenticationService} from '../../app/_services';
import {AssignedLoader} from './assigned.loader';

@Injectable()
export class AssignedLoaderService {

    private ASSIGN_URL = environment.BASIC_URL + '/loader/assign';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(loader: AssignedLoader) {
        return this.http.post(this.ASSIGN_URL, loader,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<AssignedLoader>(this.ASSIGN_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getByProjectId(id: number) {
        return this.http.get(this.ASSIGN_URL + '/project/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getByLoaderId(id: number) {
        return this.http.get(this.ASSIGN_URL + '/loader/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.ASSIGN_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.ASSIGN_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
