export interface Puesto {
    id?: number;
    name?: string;
    address?: string;
}
