import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {Route} from './route';
import {AuthenticationService} from '../../app/_services';

@Injectable()
export class RouteService {

    private ROUTE_URL = environment.BASIC_URL + '/project/route';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(route: Route) {
        return this.http.post(this.ROUTE_URL, route,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    set(route: Route) {
        return this.http.put(this.ROUTE_URL + '/' + route.id, route,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<Route>(this.ROUTE_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getByProject(id: number) {
        return this.http.get<Route>(this.ROUTE_URL + '/project/' + id,
            {
                headers: this.authService.getHeader()
            })
            .toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.ROUTE_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.ROUTE_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
