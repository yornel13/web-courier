export interface Route {
    id?: number;
    project_id?: number;
    name?: string;
    date?: string;
    time?: string;
    province?: string;
    city?: string;
    address?: string;
    latitude?: string;
    longitude?: string;
    comment?: string;
    code?: string;
    bulls?: number;
    pep?: string;
    tanager?: string;
    type?: string;
    create_at?: string;
    update_at?: string;
    status?: string;
}
