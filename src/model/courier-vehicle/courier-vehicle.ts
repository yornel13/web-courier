export interface CourierVehicle {
    id?: number;
    alias?: string;
    plate?: string;
    brand?: string;
    type?: string;
    color?: string;
    photo?: string;
}
