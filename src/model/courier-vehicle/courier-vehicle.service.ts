import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CourierVehicle } from './courier-vehicle';
import {AuthenticationService} from '../../app/_services';

@Injectable()
export class CourierVehicleService {

    private VEHICLE_URL = environment.BASIC_URL + '/courier-vehicle';

    constructor (
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(vehicle: CourierVehicle) {
        return this.http.post(this.VEHICLE_URL, vehicle,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    set(vehicle: CourierVehicle) {
        return this.http.put(this.VEHICLE_URL + '/' + vehicle.id, vehicle,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get(this.VEHICLE_URL + '/active/1',
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.VEHICLE_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.VEHICLE_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
