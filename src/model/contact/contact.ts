export interface Contact {
    id?: number;
    name?: string;
    dni?: string;
    phone?: string;
    type?: string;
    create_at?: string;
    update_at?: string;
    status?: string;
    registered_by?: number;
    company_id?: number;
}
