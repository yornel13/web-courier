export interface OrderContact {
    id?: number;
    project_id?: number;
    name?: string;
    dni?: string;
    phone?: string;
    type?: string;
    create_at?: string;
    update_at?: string;
    status?: string;
}
