import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {Contact} from './contact';
import {AuthenticationService} from '../../app/_services';

@Injectable()
export class ContactService {

    private CONTACT_URL = environment.BASIC_URL + '/contact';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}
        /**
         * De igual modo al registrar/solicitar un contacto este debe ir asociado a quien lo registra
         * (manipulacion de datos por grupo)
         * 
         */

    add(contact: Contact) {
        return this.http.post(this.CONTACT_URL, contact,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    set(contact: Contact) {
        return this.http.put(this.CONTACT_URL + '/' + contact.id, contact,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<Contact>(this.CONTACT_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.CONTACT_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.CONTACT_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
