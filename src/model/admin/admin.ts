export interface Admin {
    id?: number;
    dni?: string;
    name?: string;
    lastname?: string;
    email?: string;
    photo?: string;
    password?: string;
    create_date?: string;
    update_date?: string;
    active?: boolean;
    isAdmin?: boolean;
    rol?: string;
    registered_by?: number;
    company_id?: number;
    fire_id?: string;
    notifications?: boolean;
}
