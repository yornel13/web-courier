export interface Bitacora {
  id?: number;
  report_id?: string;
  text?: string;
  admin_id?: number;
  guard_id?: number;
  user_name?: string;
}
