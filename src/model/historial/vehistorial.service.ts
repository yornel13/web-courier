import {HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

const TOKEN = '01EC469EB5F64D8DA878042400D3CBA2';

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

@Injectable()
export class VehistorialService {

    private VEHISTORY_URL = environment.BASIC_URL + '/vehicle';

    constructor (private http: HttpClient) {}

    getAll() {
        return this.http.get(this.VEHISTORY_URL).toPromise()
            .then((response) => response);
    }

    getImei(imei: number) {
        // returnList this.http.get(this.VEHISTORY_URL + '/' + imei).toPromise()
        //     .then((response) => response);
        return this.http.get('http://dts.location-world.com/api/Fleet/' +
            'dailyhistory?token=' + TOKEN + '&' +
            'imei=' + imei + '&' +
            'year=2018&' +
            'month=09&' +
            'day=15&' +
            'timezoneoffset=-5&culture=es').toPromise()
            .then((response) => response);
    }

    getHistoryImei(imei: string) {
        return this.http.get(this.VEHISTORY_URL + '/history/' + imei + '/2020/03/10').toPromise()
            .then((response) => response);
    }

    getHistoryImeiDate(imei, year, month, day) {
        return this.http.get(this.VEHISTORY_URL + '/history/' + imei+'/'+year+'/'+month+'/'+day).toPromise()
            .then((response) => response);
    }

    async superQuery(fromDate: Date, toDate: Date, imei: String): Promise<any> {
        return new Promise<any>(async (resolve, reject) => {
            let listObtained: any = [];
            if (this.isDameDay(fromDate, toDate)) {
                const date = toDate.toISOString().split('T')[0].split('-');
                const year = date[0];
                const month = date[1];
                const day = date[2];
                listObtained = await this.getHistoryImeiDate(imei, year, month, day);
            } else {
                const diffDays = this.dateDiffInDays(fromDate, toDate);

                const fromStringDate = fromDate.toISOString().split('T')[0].split('-');
                const fYear = fromStringDate[0];
                const fMonth = fromStringDate[1];
                const fDay = fromStringDate[2];
                let fromList: any = await this.getHistoryImeiDate(imei, fYear, fMonth, fDay);

                for (let i = 1; i < diffDays; i++) {
                    const interDate = this.addDays(fromDate, i);
                    const interStringDate = interDate.toISOString().split('T')[0].split('-');
                    const iYear = interStringDate[0];
                    const iMonth = interStringDate[1];
                    const iDay = interStringDate[2];
                    const interList: any = await this.getHistoryImeiDate(imei, iYear, iMonth, iDay);
                    fromList = fromList.concat(interList);
                }

                const toStringDate = toDate.toISOString().split('T')[0].split('-');
                const tYear = toStringDate[0];
                const tMonth = toStringDate[1];
                const tDay = toStringDate[2];
                const toList: any = await this.getHistoryImeiDate(imei, tYear, tMonth, tDay);
                listObtained = fromList.concat(toList);
            }

            const records: any[] = [];
            listObtained.forEach(history => {
                const dateC = new Date(history.date + ' ' + history.time);
                if (fromDate <= dateC && dateC <= toDate) {
                    records.push(history);
                }
            });
            resolve(records);
        });
    }


    private isDameDay(d1: Date, d2: Date) {
        return d1.getFullYear() === d2.getFullYear() &&
            d1.getMonth() === d2.getMonth() &&
            d1.getDate() === d2.getDate();
    }

    private dateDiffInDays(a, b) {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }

    private addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }


}



















