export interface Record {
    date: string;
    time: string;
    latitude: number;
    longitude: number;
    address: string;
    speed: number;
    mileage: number;
    temperature_1: number;
    temperature_2: number;
    temperature_3: number;
    temperature_4: number;
    internal_battery_level: number;
    is_exception: boolean;
    alert_message: string;
    zone: string;
    index: number;
    iconUrl: string;
}
