export interface Project {
    id?: number;
    name?: string;
    description?: string;
    percentage?: number;
    create_at?: string;
    update_at?: string;
    status?: string;
    registered_by?: number;
    company_id?: number;
    shipment_route_id?: number;
    shipment_date?: string;
    shipment_time?: string;
}
