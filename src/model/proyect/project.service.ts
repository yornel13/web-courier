import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {Project} from './project';
import {AuthenticationService} from '../../app/_services';

@Injectable()
export class ProjectService {

    private PROJECT_URL = environment.BASIC_URL + '/project';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(project: Project) {
        return this.http.post(this.PROJECT_URL, project,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    set(project: Project) {
        return this.http.put(this.PROJECT_URL + '/' + project.id, project,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<Project>(this.PROJECT_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.PROJECT_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.PROJECT_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
