import { Bounds } from './bounds';

export interface ListBounds {
    data: Bounds[];
    total: number;
}