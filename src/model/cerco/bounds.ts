export interface Bounds {
    id?: number;
    imei: string;
    bounds_id?: string;
    create_at?: string;
    alias?: string;
}
