import {Guard} from './guard';
import {Project} from '../proyect/project';

export interface AssignedDriver {
    id?: number;
    driver_id?: number;
    project_id?: number;
    order_id?: number;
    create_at?: string;
    update_at?: string;
    status?: string;
    active?: boolean;
    driver?: Guard;
    project?: Project;
    order?: Project;
    vehicle_plate?: string;
    vehicle_alias?: string;
    tms_vehicle?: any;
    claro_vehicle?: any;
}
