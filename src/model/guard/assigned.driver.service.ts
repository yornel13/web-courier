import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {AuthenticationService} from '../../app/_services';
import {AssignedDriver} from './assigned.driver';

@Injectable()
export class AssignedDriverService {

    private ASSIGN_URL = environment.BASIC_URL + '/driver/assign';

    constructor(
        private http: HttpClient,
        private authService: AuthenticationService) {}

    add(driver: AssignedDriver) {
        return this.http.post(this.ASSIGN_URL, driver,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getAll() {
        return this.http.get<AssignedDriver>(this.ASSIGN_URL,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getByProjectId(id: number) {
        return this.http.get(this.ASSIGN_URL + '/project/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getByDriverId(id: number) {
        return this.http.get(this.ASSIGN_URL + '/driver/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    getId(id: number) {
        return this.http.get(this.ASSIGN_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }

    delete(id: number) {
        return this.http.delete(this.ASSIGN_URL + '/' + id,
            {
                headers: this.authService.getHeader()
            }).toPromise().then((response) => response);
    }
}
