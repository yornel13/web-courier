export interface Guard {
    id?: number;
    dni?: string;
    name?: string;
    lastname?: string;
    email?: string;
    photo?: string;
    phone?: string;
    vehicle_plate?: string;
    tms_vehicle?: any;
    claro_vehicle?: any;
    password?: string;
    create_date?: string;
    update_date?: string;
    active?: boolean;
}
