import { Alerta } from './alerta';

export interface AlertaList {
    data: Alerta[];
    total: number;
}