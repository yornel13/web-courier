import {Alerta} from './alerta';

export interface Notification {
    type?: string;
    message?: Alerta;
}