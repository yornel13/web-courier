export interface Business {
    id?: number;
    name: string;
    address?: string;
    ruc?: string;
    create_at?: string;
    update_at?: string;
    active?: boolean;
}
