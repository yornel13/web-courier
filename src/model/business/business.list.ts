import {Business} from './business';

export interface BusinessList {
    data: Business[];
    total: number;
}