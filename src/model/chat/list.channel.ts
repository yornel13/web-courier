import {Channel} from './channel';

export interface ListChannel {
    data: Channel[];
    total: number;
}