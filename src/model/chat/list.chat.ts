import {Chat} from './chat';

export interface ListChat {
    data: Chat[];
    total: number;
}