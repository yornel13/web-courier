export interface Channel {
    id?: number;
    user_id: number;
    user_type: string;
    user_name: number;
    create_at: string;
    update_at: string;
    state: number;
    channel_name: string;
    channel_id: number;
    channel_creator_id: number;
    channel_creator_type: string;
    channel_creator_name: string;
    channel_create_at: string;
    channel_update_at: string;
    channel_state: number;
    active: boolean;
}
