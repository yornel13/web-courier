export interface Member {
    id: number;
    channel_id: number;
    user_id: number;
    user_type: string;
    user_name: string;
    create_at: string;
    update_at: string;
    state: number;
}
