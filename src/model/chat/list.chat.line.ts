import {ChatLine} from './chat.line';

export interface ListChatLine {
    data: ChatLine[];
    total: number;
}