// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    ALERTS_PATH: 'courier-alert', // test real alerts
    BASIC_URL: 'https://services.tmslogcourier.com', // amazon url
    // BASIC_URL: 'http://localhost:8080', // local test url
    firebase: {
        apiKey: 'AIzaSyA87Cx0yG0b2l8Ih7zrt8rTb0lkCHXIAoI',
        authDomain: 'tms-log-courier.firebaseapp.com',
        databaseURL: 'https://tms-log-courier.firebaseio.com',
        projectId: 'tms-log-courier',
        storageBucket: 'tms-log-courier.appspot.com',
        messagingSenderId: '624767139195',
        appId: '1:624767139195:web:f3676db4ea4bd688d451de',
        measurementId: 'G-XHLCQ5Z0N8'
    },
    MONITORING_REFRESH_INTERVAL: 30000,
    VERSION: '1.03.test',
    ERROR_GENERAL: 'Ha ocurrido un error, por favor intenta mas tarde.',
    ERROR_INCORRECT: 'Algunos de los datos suministrados son incorrectos o estan incompletos, por favor verifiquelos.',
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
