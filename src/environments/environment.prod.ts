export const environment = {
    production: true,
    ALERTS_PATH: 'courier-alert',
    BASIC_URL: 'https://services.tmslogcourier.com/', // amazon url
    firebase: {
        apiKey: 'AIzaSyA87Cx0yG0b2l8Ih7zrt8rTb0lkCHXIAoI',
        authDomain: 'tms-log-courier.firebaseapp.com',
        databaseURL: 'https://tms-log-courier.firebaseio.com',
        projectId: 'tms-log-courier',
        storageBucket: 'tms-log-courier.appspot.com',
        messagingSenderId: '624767139195',
        appId: '1:624767139195:web:f3676db4ea4bd688d451de',
        measurementId: 'G-XHLCQ5Z0N8'
    },
    MONITORING_REFRESH_INTERVAL: 30000,
    VERSION: '1.03',
    ERROR_GENERAL: 'Ha ocurrido un error, por favor intenta mas tarde.',
    ERROR_INCORRECT: 'Algunos de los datos suministrados son incorrectos o estan incompletos, por favor verifiquelos.'
};
